from platform import node
from typing import List, Optional
import collections

from regex import B
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
        
class Solution:
    def isValidBST(self, root: Optional[TreeNode]) -> bool:
        
        def dfs(node: Optional[TreeNode], left, right):
            ## node is none, bst is true
            if node is None:
                return True

            ## imgaine each node within the boundary of Left and Right
            ## init the left and right are -inf < x or x < inf
            ## for each dfs on left subtree, it will leverage the inherit left boundary and use current val as right boundary to next dfs stack
            ## for each dfs on right subtree, it will leverage the inherit right boundary and use current val as left boundary to next dfs stack
            ## e.g 
            #      7
            #     4
            #    3 6
            # 7 is valid, -inf < 7 or 7 < inf
            # 4 is valid, -inf < 4 or 4 < 7 
            # 3 is valid, -inf < 3 or 3 < 4
            # 6 is valid,  4 < 6 or 6 < 7 (parent right boundary in node 4 is 7, left boundary is 4 )
            #      7
            #       9
            #      6 10
            # 7 is valid, -inf < 7 or 7 < inf
            # 9 is valid, 7 < 9 or 9 < inf 
            # 6 is invalid, 7 < 6 or 6 < 9 (parent right boundary in node 9 is 9, left boundary is 7 )
            # 10 is valid,  7 < 10 or 6 < inf
            if not(left < root.val < right):
                    return False
            
            return dfs(node.left, left, node.val) and dfs(node.right,  node.val, right) 
            
        return dfs(root, float("-inf"), float("inf"))
  




        

s=Solution()
n=TreeNode(1, TreeNode(1))

r=s.isValidBST(n)
print(r)
