from math import floor
from typing import List

class Solution:
    def search(self, nums: List[int], target: int) -> int:
        left, right = 0, len(nums)-1
        while left <= right:
            guess = left + (right - left ) // 2
            if nums[guess] == target: return guess
            if nums[guess] < target:
                left = guess+1
            else:
                right = guess-1
        return -1

s=Solution()
n=[1,2,3,4,5,9,12,13,14]
r=s.search(n,9)
print("result",r)
