from typing import List, Optional
import collections
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

# Bfs the tree , use lv to categorize the tree, if there is a item at the same lv, then skip, scan from right to left
# O(n) run time, O(n) space 
class Solution:
    def rightSideView(self, root: Optional[TreeNode]) -> List[int]:
        res = []
        if root is None: return res
        stack = collections.deque([(0, root)])
        d = {}
        while stack:
            lv , node = stack.popleft()

            if lv not in d and node:
                d[lv] = node.val
                res.append(node.val)

            if node.right:
                stack.append((lv+1, node.right))
            if node.left:
                stack.append((lv+1, node.left))
            
        return res

class Solution2:
    def rightSideView(self, root: Optional[TreeNode]) -> List[int]:
        q = collections.deque([root])
        res = []
        while q: 
            right = None
            for i in range(len(q)):
                c = q.popleft()
                if c:
                    # instead of putting val to right, it is better to use node itself, right either = None or Node
                    # with val, you have to convert type (other lang), and if right is not correct for 0 when right is int
                    right = c 
                    q.append(c.left)
                    q.append(c.right)

            if right:
                res.append(right.val)
        return res    

s=Solution2()
n=TreeNode(9, TreeNode(3, TreeNode(6, TreeNode(7,TreeNode(10)))), TreeNode(8, TreeNode(2,TreeNode(4), TreeNode(5))))
print(s.rightSideView(n))
