from typing import List


class Solution2:
    def maxSubArray(self, nums: List[int], ans:int) -> List[int]:
        leftPointer = 0
        maxRange = []
        dis = 0
        for i in range(1, len(nums)):
            if nums[i] + nums[i-1] <= ans:
               nums[i]+=nums[i-1]
            else: 
                leftPointer = i
            maxDis = max(dis, i-leftPointer)
            if maxDis != dis: 
                maxRange=[leftPointer,i]
                dis = maxDis
        return maxRange


s=Solution2()
r=s.maxSubArray([1,2,3,4,5,0,0,0,6,7,8,9,10], 15)
print(r)
