from typing import List

class Solution:
    def maxArea(self, height: List[int]) -> int:
        L, R = 0, len(height) - 1
        maxVal =0
        while L < R :
            maxVal = max(maxVal , (R-L) * min(height[R],height[L]))
            if height[L] < height[R]:
                L += 1
            else:
                R -= 1
        return maxVal
        
s=Solution()
print(s.maxArea([2,1]))
