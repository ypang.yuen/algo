from typing import List

class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        d = {}
        for i in range(len(nums)):
            x = nums[i]
            r = target -x
            if r in d:
              return [d[r], i]
            d[x] = i

s = Solution()
r=s.twoSum([3,3], 6)
print(r)