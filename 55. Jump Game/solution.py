import collections
from typing import List
# 1. DFS can solve but timeout, using greedy can be faster and easier to understand
# 2. From back to front, see if current index + jump counter can >= last index, if yes change last index to current index
# 3. at the end, if it can reach to zero, that means it is a valid path
class Solution:
    def canJump(self, nums: List[int]) -> bool:
        lastIndex = len(nums) - 1

        for i in range(len(nums)-1, -1,-1):
            if i + nums[i] >= lastIndex:
                lastIndex = i

        return lastIndex == 0

S=Solution()
print(S.canJump([2,0,0,0,4]))
print(S.canJump([2,1,1,2,4]))