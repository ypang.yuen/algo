
import collections
from typing import List
import heapq

## input [-1,1,4,-5,6,-2,5] find max val
def kadane(nums: list[int]):
    for i in range(1,len(nums)):
        prev = nums[i-1]
        cur = nums[i]
        # update cur amt with kadane algo
        if(prev+cur >= cur):
            nums[i] = prev+cur
        
        ## do your task 