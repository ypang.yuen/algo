
import collections


def slideWindow(s: str, search: str):
    d1 = collections.defaultdict(int)
    d2 = {}
    left = 0
    for c in search:
        d1[c] += 1

    for i in range(len(s)):
        c = s[i]
        d2[s] = d2.get(s, 0) + 1
        
        # check if the window valid (compare 2 hash maps)
        valid=True
        for x in d1:
            if x not in d2 or d2[x] < d1[x]:
                valid = False
                break
        
        # make it become invalid if valid, so we can move the pointer forward
        while valid:
            # do calculation

            # do calculation
            
            newC = s[left]

            if newC in d2:
                d2[newC] -= 1
                if d2[newC] <= 0:
                    d2.pop(newC)
            
            left += 1 

            # reduce the item from d2, until it become invalid
            for x in d1:
                if x not in d2 or d2[x] < d1[x]:
                    valid = False
                    break