
import collections
from typing import List
import heapq

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


## bfs Binary Tree 1, with passing lv to bottom tree to regonize it is same leve
def bfsBinaryTreeWithQ1(root:TreeNode):
    stack = collections.deque([(0, root)])
    while stack:
        lv , node = stack.popleft()
        # do whatever you want
        
        if node.left:
            stack.append((lv+1, node.left))
        if node.right:
            stack.append((lv+1, node.right))


## bfs Binary Tree 2, find all adj node in each iteration to regonize it is same leve
def bfsBinaryTreeWithQ1(root:TreeNode):
    stack = collections.deque([root])
    while stack:
        l = len(stack)
        for i in range(l):
            node = stack.popleft()
            # add adj of the same lv to q
            if node :
                stack.append(node.left)
                stack.append(node.right)
        # do whatever you want