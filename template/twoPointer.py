
import collections
from typing import List

def twoPointers(nums: List[int]):
    L, R = 0 , len(nums) - 1

    while L < R: 

        # move pointer base on condition
        
        if L < R:
            L+=1
        else:
            R+=1

        