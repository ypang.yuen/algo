
import collections
from typing import List
import heapq

## input [[2,15], [1,4], [8,12]]
def linearSearch(nums: list[list[int]]):
    stack = []
    ## sort if give array not sort
    for n in nums:
        heapq.heappush(stack, (n[0], 1)) ## heap will sort the first param from tuple
        heapq.heappush(stack, (n[1], -1))

    total = 0 
    while stack:
        time, count = stack.pop()
        total+=count
        if total > 1:
            print("something overlap")