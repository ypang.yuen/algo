
import collections
from typing import List

def binarySearch(nums: List[int], target: int):
    L, R = 0 , len(nums) - 1

    while L <= R: # if search from an array, use <= ,
        M= R+(L-R)//2
        mid = nums[mid]

        if nums[M] == target :
            # do whatever you wanna
            print(M)
            # do whatever you wanna

        if nums > target:
            R = M - 1
        else:
            L = M + 1

        