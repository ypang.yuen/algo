
import collections
from typing import List, Optional
import heapq

class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

## Palindrome link list

def isPalindrome(head: Optional[ListNode]):
    rev = None
    slow = fast = head

    while fast and fast.next:
        fast= fast.next.next
        rev, rev.next, slow = slow, rev, slow.next
    
    if fast: 
        slow = slow.next # move foward if head length is odd
    
    while rev:
        if rev.val != slow.val:
            return False
        rev = rev.next
        slow = slow.next
    return True

n=ListNode(1)
n.next=ListNode(2)
n.next.next=ListNode(3)
n.next.next.next=ListNode(3)
n.next.next.next.next=ListNode(2)
n.next.next.next.next.next=ListNode(1)
print(s.isPalindrome(n))