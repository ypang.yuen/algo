import collections
from typing import Optional

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def diameterOfBinaryTree(self, root: Optional[TreeNode]) -> int:
        self.da = 0
        def dfs(node:TreeNode, c):
            if not node: return 0
            c+=1
            left = dfs(node.left, c) ## max depth of left
            right = dfs(node.right,c) ## max depth of right
            
            self.da = max(self.da, left+right) ## is max left + right on this subtree is the longest
            
            return max(left,right)+1 ## return max depth of the tree
        
        dfs(root, 0)
        return self.da

 

s = Solution()
n=TreeNode(-3)
n.left = TreeNode(-9)
n.right = TreeNode(-3)

n.right.left = TreeNode(-4)

n.left.left = TreeNode(9)
n.left.left.left = TreeNode(6)
n.left.left.left.left = TreeNode(0)
n.left.left.left.right = TreeNode(6)
n.left.left.left.left.left = TreeNode(-1)
n.left.left.left.right.right = TreeNode(-4)

n.left.right = TreeNode(-7)
n.left.right.left = TreeNode(-6)
n.left.right.right = TreeNode(-6)
n.left.right.left.left = TreeNode(5)
n.left.right.right.left = TreeNode(9)
n.left.right.right.left.left = TreeNode(-2)
r=s.diameterOfBinaryTree(n)
print(r)

