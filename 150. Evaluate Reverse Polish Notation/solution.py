from typing import List


class Solution:
    def evalRPN(self, tokens: List[str]) -> int:
        d= {
            "+": lambda a, b: a + b,
            "-": lambda a, b: a - b,
            "/": lambda a, b: int(a / b),
            "*": lambda a, b: a * b
        }
        p = 0
        while len(tokens) - 1:
            while tokens[p] not in "+-*/":
                p += 1
            opt = tokens[p]
            v2 = int(tokens[p-1])
            v1 = int(tokens[p-2])
            ans = d[opt](v1,v2)
            tokens[p-1] = ans
            tokens.pop(p)
            tokens.pop(p-2)
            p-=1
        return tokens[0]

s=Solution()
#print(s.evalRPN(["2","1","+","3","*"]))
print(s.evalRPN(["4","-2","/","2","-3","-","-"]))

