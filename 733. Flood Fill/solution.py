import collections
from typing import List

class Dfs:
    def dfs(self, y,x):
        self.image[y][x] = self.newColor
        for Y,X in [(y, x+1),(y, x-1),(y+1, x), (y-1, x)]:
            if 0 <= Y < self.h and 0 <= X < self.w and self.image[Y][X] == self.color:
               self.dfs(Y,X)


    def floodFill(self,image:List[List[int]],sr:int,sc:int,newColor:int)->List[List[int]]:
        self.w,self.h,self.color = len(image[0]),len(image), image[sr][sc]
        if self.color == newColor : return image
        self.newColor = newColor
        self.image = image
        self.dfs(sr,sc)
        return image
        ## y,x


class Bfs:
    def floodFill(self,image:List[List[int]],sr:int,sc:int,newColor:int)->List[List[int]]:
        w,h,color = len(image[0]),len(image), image[sr][sc]
        if color == newColor : return image
        ## y,x
        q = collections.deque([(sr,sc)])
        while q:
            y,x = q.popleft()
            image[y][x] = newColor
            for Y,X in [(y, x+1),(y, x-1),(y+1, x), (y-1, x)]:
                if 0 <= Y < h and 0 <= X < w and image[Y][X] == color:
                    q.append((Y,X))
        return image

s=Dfs()
r=s.floodFill([[1,1,1,1,1,0],[1,1,1,1,0,0],[0,1,1,1,0,1]], 1, 1, 2)
print(r)

# why dfs is faster