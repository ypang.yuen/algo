
import collections
from typing import List,Optional

class Solution:
    def asteroidCollision(self, asteroids: List[int]) -> List[int]:
        stack = []

        for n in asteroids:
            # we only compare when:
            # there is a number in stack
            # and we only compare when last item is positive , and current item is negative
            while stack and stack[-1] >= 0 and n < 0:
                last = stack[-1]
                if abs(n) > last:
                    # if current is larger than last item, we pop last item , and use curr item to continue
                    stack.pop()
                    continue
                elif abs(n) == last:
                    # if current equal last item, we pop last itme, but we stop there
                    stack.pop()
                break
            else:
                stack.append(n)

        return stack
S = Solution()

print(S.asteroidCollision([-10,10,5,-7,-11]))