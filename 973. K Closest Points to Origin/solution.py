from cgitb import strong
import collections
from typing import List, Optional
from heapq import heapify, _heapify_max, _heappop_max, _heapreplace_max, heappush, heappop
## use heap, which is O(n+klogn) run time, heap is O(n), heap push and pop cost log n, 
class Solution:
    def kClosest(self, points: List[List[int]], k: int) -> List[List[int]]:
        heap = []
        for p in points:
           heap.append((p[0] ** 2 + p[1] ** 2, p))
        heapify(heap)
        res = []
        while k > 0:
            i, p = heappop(heap)
            res.append(p)
            k-=1

        return res

## use heap, which is O(nlogn) run time, heap is O(n)
class Solution2:
    def kClosest(self, points: List[List[int]], k: int) -> List[List[int]]:
        points.sort(key=lambda x: x[0] ** 2 + x[1] ** 2)
        return points[:k]

s = Solution()
print(s.kClosest([[3,3],[5,-1],[-2,4]], 2))