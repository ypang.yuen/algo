
import collections
from ctypes.wintypes import tagMSG
from typing import List,Optional
from bisect import bisect_left, bisect_right
import heapq
import bisect
from regex import F
class Trie:
    def __init__(self):
        self.child = {}
        self.end = False

class Solution:
    def suggestedProducts(self, products: List[str], searchWord: str) -> List[List[str]]:
        products.sort()
        output = []
        txt = ''
        i=0
        for c in searchWord:
            txt+=c
            arr = []
            i = bisect.bisect_left(products, txt, i)
            for c in products[i:i+3]:
                if c.startswith(txt):
                    arr.append(c)
            output.append(arr)
        return output

        
# Your Solution object will be instantiated and called as such:
s = Solution()
print(s.suggestedProducts(
["mobile","mouse","moneypot","monitor","mousepad"],
"mouse"
))
