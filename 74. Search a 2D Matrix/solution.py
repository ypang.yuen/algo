from operator import truediv
from typing import List

class Solution:
    def searchMatrix(self, matrix: List[List[int]], target: int) -> bool:
        # newArr = []
        w, h = len(matrix[0]), len(matrix)
        # for x in matrix:
        #     newArr += x
        l, r = 0, (w*h) - 1
        while l <= r:
            guess = (l+r)//2
            mid = matrix[guess//w][guess%w]
            print(mid)
            if mid == target:
                return True
            if mid < target:
                l = guess + 1
            else:
                r = guess 
        
        return False

    def searchMatrix2(self, matrix: List[List[int]], target: int) -> bool:
        H, W = len(matrix) , len(matrix[0])
        
        row = 0
        found = False
        L, R = 0, H-1
        while L <= R:
            M = (L+R) // 2

            start = matrix[M][0]
            end = matrix[M][-1]

            if start <= target <= end:
                found = True
                row = M
                break

            if end < target:
                L = M+1
            else:
                R = M-1
        
        if not found:
            return False
            
        L, R = 0, W-1
        while L <= R:
            M = (L+R) // 2

            mid = matrix[row][M]

            if mid == target:
                return True

            if mid < target:
                L = M+1
            else:
                R = M-1

        return False
      
        
s=Solution()
print(s.searchMatrix([[1,3,5,7],[10,11,16,20],[23,30,34,60]], 3))