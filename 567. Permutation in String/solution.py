import collections
from typing import List

#space O(26)
#run O(M) + O(N)
class Solution:
    def checkInclusion(self, s1: str, s2: str) -> bool:
        d1 = collections.defaultdict(int)
        d2 = collections.defaultdict(int)
        for c in s1:
            d1[c] += 1

        start = 0
        for i in range(0, len(s2)):
            d2[s2[i]] += 1

            if i < len(s1) - 1: 
                continue

            if d2 == d1 : return True

            d2[s2[start]] -= 1
            if d2[s2[start]] <= 0:
                del d2[s2[start]]
            start+=1

        return False
            

       

s=Solution()
print(s.checkInclusion( "ab", "eidbaooo"))
print(s.checkInclusion( "aa", "dcda"))
