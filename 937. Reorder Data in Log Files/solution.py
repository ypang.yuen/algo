from cgitb import strong
import collections
from typing import List, Optional
class Solution:
    def reorderLogFiles(self, logs: List[str]) -> List[str]:
        letterLog = []
        digitLog = []
        
        for l in logs:
            arrLog = l.split(' ')
            id = arrLog[0]
            content = arrLog[-1]
            
            if content.isdigit():
                digitLog.append(l)
            else:
                letterLog.append((' '.join(arrLog[1::]),id))
        
        letterLog.sort(key=lambda x: (x[0], x[1]))
        
        return [id + ' ' + content for content,id in letterLog]+digitLog