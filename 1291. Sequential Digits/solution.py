import collections
from typing import List,Optional
import heapq
# Math Way
class Solution:
    def sequentialDigits(self, low: int, high: int) -> List[int]:
        output = []
       
        start = low

        def getFirstDigit(digit):
            return int(str(digit)[0])

        def calDigit(digit, n):
            for i in range(n-1):
                lastDigit = (digit+1)%10
                digit*=10
                digit+=lastDigit
                if lastDigit == 9:
                    return (digit, 1)
            return (digit, 0)
            
        first = getFirstDigit(start)
        length = len(str(start))
        maxLength = 10
        while start <= high and length < maxLength:
            start, carry = calDigit(first, length)
            if carry > 0:
                length+=1
                first = 1
            else:
                first+=1
            if start>= low and start <= high:
                output.append(start)
        return output
       
class Solution:
    def sequentialDigits(self, low: int, high: int) -> List[int]:
        sample = "123456789"
        maxLen = 10
        output = []
        for i in range(len(low), len(high)+1):
            for j in range(maxLen-i):
                print(j)
s=Solution()
print(s.sequentialDigits(10,1000000000))


