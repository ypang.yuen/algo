class Solution:
    def isPalindrome(self, x: int) -> bool:
        n2 = x
        n= 0
        while x > 0:
            n = n*10 + x%10
            x = x//10
        return n == n2

s=Solution()
print(s.isPalindrome(1221))

