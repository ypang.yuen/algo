from typing import List, Optional
import collections
import sys
class Solution:
    def findMin(self, nums: List[int]) -> int:
        L, R = 0, len(nums) - 1
        A = (sys.maxsize * 2) +1
        while L <= R:
            if nums[L] < nums[R]:
                A = min(nums[L] ,A)
                break
            M = (L+R) // 2
            mid = nums[M]
            A = min(mid ,A)
            if nums[R] > mid:
                R = M-1
            else:
                L = M+1

        return A

# 9 10 1 2 3 4 6


s=Solution()
print(s.findMin([9,3,4,6,7,8]))