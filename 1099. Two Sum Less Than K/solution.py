
from typing import  List,Optional
import collections
#O(n)
class Solution:
    def twoSumLessThanK(self, nums: List[int], k: int) -> int:
        nums.sort()
        L = 0
        R = len(nums)-1
        ans = -1
        while L < R:
            left = nums[L]
            right = nums[R]
            total = left + right
            if total < k:
                ans = max(total, ans)
            if total >= k:
                R-=1
            else:
                L+=1
        return ans
                
            
            
