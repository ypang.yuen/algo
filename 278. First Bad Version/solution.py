from typing import Optional, List
# The isBadVersion API is already defined for you.
bad = 20
def isBadVersion(version: int) -> bool:
  return version >= bad
class Solution:
    def firstBadVersion(self, n: int) -> int:
      left, right = 1, n
      while left < right:
        c+=1
        guess = (left+right) // 2
        if isBadVersion(guess):
          right = guess
        else:
          left = guess+1
      return left
         
s=Solution()
r=s.firstBadVersion(19023)
print(r)


def find_boundary(arr: List[bool]) -> int:
    if arr[0] == True : return 0
    left , right =0, len(arr)
    ans = -1
    while left <= right:
        guess = (left+right) // 2
        if arr[guess]:
            right = guess-1
            ans = guess
        else:
            left = guess+1
    return ans


print(find_boundary([False,False,False,False,False, True,True,True,True]))
