from typing import List, Optional
import collections
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

# BFS scan each level of tree, time: O(n), Space: O(n)

# start the node with queue

# find all node with same level by for loop with the len of the current queue
# e.g [1,2,3,4,5,6,7] binary tree
# start from node 1
# q len = 1, loop 1 time
# pop item from q = node 1
# add 2, 3 to q
# q len = 2, loop 2 time
# pop item 2
# add child of 2 to q, 4,5
# pop item 3
# add child of 3 to q, 6,7
# q len = 4, loop 4 time
# none of them hv child stop
class Solution:
    def levelOrder(self, root: Optional[TreeNode]) -> List[List[int]]:
        q = collections.deque([root])
        res = []
        while q:
            a = []
            for i in range(len(q)):
                c =  q.popleft()
                if c:
                    a.append(c.val)
                    q.append(c.left)
                    q.append(c.right)
            if(len(a)):
                res.append(a)
        return res
                
    
        

s=Solution()
n=TreeNode(9, TreeNode(3, TreeNode(2,TreeNode(4), TreeNode(5))), TreeNode(3, TreeNode(6)))

r=s.levelOrder(n)
print(r)
