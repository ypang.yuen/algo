
from ast import Lambda
import collections
from typing import List,Optional
import heapq
import random
class Solution:
    def eraseOverlapIntervals(self, intervals: List[List[int]]) -> int:
        intervals.sort(key=lambda x : (x[1],x[0]))
        prev = intervals[0]
        res = 0
        for i in range(1, len(intervals)):
            curr = intervals[i]
            if prev[1] > curr[0]:
                res += 1
            else:
                prev = curr
        return res

s=Solution()
print(s.eraseOverlapIntervals([[0,2],[1,3],[2,4],[3,5],[4,6]]))
 # while head:
            
        #     head = head.next
             


