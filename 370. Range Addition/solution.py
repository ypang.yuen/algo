

from typing import  List,Optional
import collections
#O(n)
class Solution:
    def getModifiedArray(self, length: int, updates: List[List[int]]) -> List[int]:
        bucket = [0] * (length + 1)
        for s,e,i in updates:
            bucket[s] += i
            if e+1 <= length-1:
                bucket[e+1] -= i
        s = 0
        for i in range(len(bucket)):
            s += bucket[i]
            bucket[i] = s
        bucket.pop()
        return bucket

s = Solution()
a= 6
b= [[1,3,2],[2,4,3],[0,2,-2]]

print(s.getModifiedArray(a,b))


