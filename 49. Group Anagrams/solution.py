import collections
from typing import List
class Solution:
    def groupAnagrams(self, strs) -> List[List[str]]:
        d = collections.defaultdict(list)
        for s in strs:
            key = list(s)
            key.sort()
            d[tuple(key)].append(s)
        return d.values()
s=Solution()
print(s.groupAnagrams(["eat","tea","tan","ate","nat","bat"]))


