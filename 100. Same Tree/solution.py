from typing import Optional
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
        
class Solution:
    def isSameTree(self, p: Optional[TreeNode], q: Optional[TreeNode]) -> bool:
        if p is None and q is None: return True
        
        if p is None or q is None or p.val != q.val:
            return False
        
        return self.isSameTree(p.left, q.left) and self.isSameTree(p.right, q.right)

s=Solution()
n=TreeNode(1, TreeNode(2,TreeNode(4), TreeNode(5)), TreeNode(3, TreeNode(6)))
n2=TreeNode(1, TreeNode(2,TreeNode(4), TreeNode(5)), TreeNode(3, TreeNode(6)))
r=s.isSameTree(n, n2)
print(r)