from typing import List, Optional
import collections
import heapq

class KthLargest:

    def __init__(self, k: int, nums: List[int]):
        self.k = k
        self.nums = nums
        heapq.heapify(self.nums)
        while len(self.nums) > k:
            heapq.heappop(self.nums)

    def add(self, val: int) -> int:
        heapq.heappush(self.nums, val)
        if len(self.nums) > self.k:
            heapq.heappop(self.nums)
        return self.nums[0]
# Your KthLargest object will be instantiated and called as such:
obj = KthLargest(2, [4,5,8,2])
print(obj.add(3))
print(obj.add(4))
print(obj.add(2))
print(obj.add(7))
print(obj.add(8))

print(-2 - (-2))