import collections
from typing import List

class Solution:
        # use slide window to find the max window
        def characterReplacement(self, s: str, k: int) -> int:
            L = 0
            hash = collections.defaultdict(int)
            maxF = 0
            maxLen = 0
            
            for i in range(len(s)):
                c = s[i]
                # we store char freq and use to calculate the max freq
                hash[c] += 1
                # we store the max freq and use to calculate the number needed to replace in a window
                maxF = max(maxF, hash[c])

                windowSize = i - L + 1
                # windowSize - MaxF = number of char needed to replace in a window, becoz we use non freq char to replace in each window
                if windowSize - maxF > k:
                    # if the replacement has excited k, then we need to shrink the size of the window by move L pointer forward, 
                    # in the mean time, we also need to decrease the freq hash of the char in L pointer
                    # but we can keep maxFreq, as it doesnt affect the result
                    hash[s[L]] -=1
                    L+=1
                else:
                    maxLen = max(maxLen, windowSize)
            return maxLen     
       

s=Solution()
print(s.characterReplacement("AABABCBBBB", 2))
#print(s.characterReplacement("ABABBA", 2))
# print(s.characterReplacement("AABABCBA", 3))
#print(s.characterReplacement("A",1))
