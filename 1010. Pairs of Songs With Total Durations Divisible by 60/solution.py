from cgitb import strong
import collections
from typing import List, Optional
from heapq import heapify, _heapify_max, _heappop_max, _heapreplace_max, heappush, heappop
class Solution:
    def numPairsDivisibleBy60(self, time: List[int]) -> int:
        hashMap = collections.defaultdict(int)
        ans = 0
        for t in time:
            remain = t%60
            pair = 60 - remain 
            if remain == 0:
                pair = 0
            if pair in hashMap:
                ans += hashMap[pair]
            hashMap[remain] += 1
            
            
        return ans