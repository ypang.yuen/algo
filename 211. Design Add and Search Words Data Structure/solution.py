import collections

# Create a Trie Class, Prefix Tree
class TrieNode:
    def __init__(self):
        self.child = {}
        self.end = False

class WordDictionary:
    def __init__(self):
        self.root = TrieNode()

    def addWord(self, word: str) -> None:
        # use cur as Cursor
        cur = self.root
        for c in word:
            if c not in cur.child:
                cur.child[c] = TrieNode()
            cur = cur.child[c]
        cur.end = True

    
    def search(self, word: str) -> bool:
        def dfs(cur, pos = 0):
            for i in range(pos, len(word)):
                c = word[i]
                # only wildcart need recursive look up, becasue we need to search all prefix tree 
                if c == '.':
                    for child in cur.child.values():
                        # if one of the tree found, then we can return and move to next char
                        if dfs(child, i+1):
                            return True
                    return False
                else:
                    # we dont need run recursive
                    if c not in cur.child:
                        return False
                    cur  = cur.child[c]
            # The end indicate a word "aabb", and search "aab" will return false, as the loop of word has end, but the cur is not end
            return cur.end 
        return dfs(self.root)

obj = WordDictionary()
obj.addWord("bad")
obj.addWord("dad")
obj.addWord("mad")

print( obj.search("pad") )
print( obj.search("bad") )
print( obj.search(".ad") )
print( obj.search("b..") )
