
import collections
from heapq import merge

from typing import List,Optional
#
class Solution:
    def letterCombinations(self, digits: str) -> List[str]:
        output = []
        hash = {"2": "abc", "3": "def", "4": "ghi", "5": "jkl", 
                   "6": "mno", "7": "pqrs", "8": "tuv", "9": "wxyz"}

        if len(digits) == 0: return output

        def dfs(pos = 0, arr = ''):
            if len(arr) == len(digits):
                output.append(arr)
                return
            let = hash[digits[pos]]
            for l in let:
                dfs( pos+1, arr+l)
        
        dfs()

        return output 

S = Solution()
print(S.letterCombinations("23"))