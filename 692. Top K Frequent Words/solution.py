
import collections
from heapq import heapify, merge,_heapify_max,_heappop_max
from typing import List,Optional

class Solution:
    def topKFrequent(self, words: List[str], k: int) -> List[str]:
        arr = [[]] * (len(words) +1)
        output = []
        words.sort()
        
        q = collections.Counter(words)
        for word,counter in q.items():
            if len(arr[counter]) == 0:
                arr[counter] = []
            arr[counter].append(word)
        
        for i in range(len(arr)-1,-1,-1):
            wordList = arr[i]
            if len(wordList) == 0:
                continue
            for word in wordList:
                if len(output) < k:
                    output.append(word)
            if len(output) == k:
                return output
        return output

S = Solution()
n = ["the","day","is","an","the","the","the","an","is"]
print(S.topKFrequent(n, 4))

