
from ast import Lambda
import collections
from typing import List,Optional
import heapq
class Solution:
    def solve(self, grid: List[List[str]]) -> None:
        H, W = len(grid), len(grid[0])
        def dfs(r,c):
            grid[r][c] = '#'
            for Y,X in [(r+1, c),(r-1, c),(r, c+1),(r, c-1)]:
                if 0 <= Y < H and 0 <= X < W and grid[Y][X] == "O":
                    dfs(Y,X)

        for r in range(H):
            for c in range(W):
                if (r == 0 or r == H-1 or c == 0 or c == W-1) and grid[r][c] == "O":
                    dfs(r,c)
                
        for r in range(H):
            for c in range(W):
                if grid[r][c] == "O":
                    grid[r][c] = 'X'
                if grid[r][c] == "#":
                    grid[r][c] = 'O'
        
s=Solution()

a = [["X","X","X","X"],["X","O","O","X"],["X","X","O","X"],["X","O","X","X"]]
print(s.solve(a))


             


