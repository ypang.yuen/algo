from ctypes import pointer
from curses.ascii import SO
from typing import List, Optional

class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def mergeTwoLists(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        res = temp = ListNode(0)

        while l1 and l2 :
            if l1.val > l2.val:
                temp.next = l2
                l2 = l2.next
            else:
                temp.next = l1
                l1 = l1.next
                
            temp = temp.next
        if l1: temp.next = l1
        if l2: temp.next = l2  

        return res.next


s=Solution()

n1 = ListNode(1)
n1.next = ListNode(2)
n1.next.next = ListNode(4)
n1.next.next.next = ListNode(5)
n1.next.next.next.next = ListNode(18)

n2 = ListNode(2)
n2.next = ListNode(8)
n2.next.next = ListNode(9)
n2.next.next.next = ListNode(22)
n2.next.next.next.next = ListNode(28)

r=s.mergeTwoLists(n1, n2)

while r:
  print(r.val)
  r = r.next