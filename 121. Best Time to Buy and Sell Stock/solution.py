from typing import List

class Solution:
    def maxProfit(self, prices: List[int]) -> int:
       buy=prices[0]
       profit=0
       for p in prices:
            profit = max(profit, p-buy)
            buy = min(p, buy)
       return profit



s = Solution()
r=s.maxProfit([0, 6, -3, 7])

print(r)

r=s.maxProfit([7,6,4,2,3,1,1])
print(r)

## https://leetcode.com/problems/best-time-to-buy-and-sell-stock/discuss/39038/Kadane's-Algorithm-Since-no-one-has-mentioned-about-this-so-far-%3A)-(In-case-if-interviewer-twists-the-input)
