
import collections
from typing import List,Optional

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution:
    def widthOfBinaryTree(self, root: Optional[TreeNode]) -> int:
        q  = collections.deque([(root, 0)])
        maxWidth = 0
        while(q):
            # find first index
            _, leadIndex = q[0]
            for i in range(len(q)):
                c, index = q.popleft()
                if c.left:
                    # if there is a left tree, it means next index can start at least multiply current index by 2
                    # if current index is 0, next should be 0
                    # if current index is 1, next should be 1*2
                    q.append((c.left, 2*index))
                
                if c.right:
                    # if there is a right tree, it means next index can start at least multiply current index by 2 +1
                    # if current index is 0, next should be 1
                    # if current index is 1, next should be 1*2 + 1 = 3
                    q.append((c.right, 2*index+1))

            # cal the distance from the lead index to end index
            maxWidth = max(maxWidth, index - leadIndex +1)
        return maxWidth



S=Solution()
n=TreeNode(1, TreeNode(2,TreeNode(4, TreeNode(8)), TreeNode(5)), TreeNode(3, TreeNode(6, None, TreeNode(7))))
S.widthOfBinaryTree(n)
