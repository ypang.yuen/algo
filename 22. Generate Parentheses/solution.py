
from typing import List,Optional
class Solution:
    def generateParenthesis(self, n: int) -> List[str]:
        output = []
        
        def dfs(open, close, arr = []):
           
            if len(arr) == n*2:
                output.append(''.join(arr))
                return

            if open < n: 
                arr.append("(")
                dfs(open+1,close, arr)
                arr.pop()

            if close < n and close < open: 
                arr.append(")")
                dfs(open,close+1, arr)
                arr.pop()

        dfs(1, 0, ["("])

        return output

S = Solution()

print(S.generateParenthesis(3))