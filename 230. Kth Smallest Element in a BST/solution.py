from platform import node
from typing import List, Optional
import collections

from regex import B
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

## Inccursive Inorder Traversals (Left, Root, Right)
class Solution2:
    def kthSmallest(self, root: TreeNode, k: int) -> int:
        stack = []
        curr = root
        
        while stack or curr:
            while curr:
                stack.append(curr)
                curr = curr.left
            curr = stack.pop()
            k -= 1
            if k == 0:
                return curr.val
            curr = curr.right     

## Recursive Inorder Traversals (Left, Root, Right) with global var
class Solution:
    def kthSmallest(self, root: Optional[TreeNode], k: int) -> int:
        self.kthSmallest = root.val
        self.k = k
        def dfs(root, k):
            if self.k <= 0 or not root: return
            dfs(root.left, k)
            self.k-=1
            if self.k == 0:
                self.kthSmallest = root.val
                return
            dfs(root.right, k)
        dfs(root, k)
        return self.kthSmallest

s=Solution2()
n=TreeNode(3, TreeNode(1, None, TreeNode(2)), TreeNode(4))

r=s.kthSmallest(n, 1)
print(r)
