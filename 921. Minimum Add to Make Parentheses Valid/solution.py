import collections
from typing import List,Optional
import heapq
# Stack
class Solution:
    def minAddToMakeValid(self, s: str) -> int:
        stack = []
        for c in s:
            if len(stack) > 0 and stack[-1] == '(' and c == ')':
                stack.pop()
            else:
                stack.append(c)
        
        return len(stack)

#O(1) space
class Solution:
    def minAddToMakeValid(self, s: str) -> int:
        bal = ans = 0
        for c in s:
            if c == "(":
                bal+=1
            else:
                bal-=1
            if bal < 0:
                bal+=1
                ans+=1
        
        return bal+ans

 
s=Solution()
a =  "()))(("
print(s.minAddToMakeValid(a))


