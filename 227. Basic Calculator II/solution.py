
import collections
from typing import List,Optional

class Solution:
   
    def calculate(self, s: str) -> int:
        stack = []
        lastOper = '+'
        numStr = ''
        
        def getVal(lastOper, currNum):
            if lastOper == '*' or lastOper == '/':
                lastVal = stack.pop()
                if lastOper == '*':
                    return lastVal * currNum
                else:
                    return abs(lastVal) // currNum * (1 if lastVal >= 0 else -1)
            else:
                return -currNum if lastOper == '-' else currNum
            
        for i in range(len(s)):
            c = s[i]
            if c == ' ':
                continue
            if c.isdigit():
                numStr+=c
                continue
            val = getVal(lastOper, int(numStr))
            stack.append(val)
            lastOper = c
            numStr = ''
        
        val = getVal(lastOper, int(numStr))
        stack.append(val)
        return sum(stack)

# without stack
class Solution:
    def calculate(self, s: str) -> int:
        lastOper = '+'
        numStr = ''
        total = 0
        last = 0
        for i in range(len(s)):
            c = s[i]
            if c.isdigit():
                numStr+=c
            if not c.isdigit() and c != ' ' or i == len(s)-1: 
                currNum = int(numStr)
                if lastOper =='+' or lastOper == '-':
                    total+=last
                    last = -currNum if lastOper == '-' else currNum
                elif lastOper == '*':
                    last = last * currNum
                elif lastOper == '/':
                    last = abs(last) // currNum * (1 if last >= 0 else -1)
                lastOper = c
                numStr = ''
        total += last
        return total
             
s=Solution()

print(s.calculate("3+5/2"))


