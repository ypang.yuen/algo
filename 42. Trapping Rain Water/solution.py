from typing import  List,Optional
import collections
#O(n)
class Solution:
    def trap(self, height: List[int]) -> int:
        L = 0 
        R = len(height)-1
        ans = RMax = LMax = 0
        while L < R:
            left = height[L]
            right = height[R]
            if left < right:
                if left >= LMax:
                    LMax = left
                else:
                    ans+=(LMax-left)
                L+=1
            else:
                if right >= RMax:
                    RMax = right
                else:
                    ans+=(RMax-right)
                R-=1  
        return ans  
s = Solution()
a= [0,1,0,2,1,0,1,3,2,1,2,1]
print(s.trap(a))


