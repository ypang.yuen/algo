
import collections
from heapq import merge

from typing import List,Optional
# slide window question, O(Nx26) runtime, O(N) space 
# similar to 567. Permutation in String
class Solution:
    def findAnagrams(self, s: str, p: str) -> List[int]:
        hash = collections.Counter(p)
        tmp = collections.defaultdict(int)
        L, R =0, len(s) 

        output = []
        for i in range(0,R):
            char = s[i]
            # store char in tmp for slide window compare
            tmp[char] += 1

            # slide window start from len of p
            if i < len(p) - 1: 
                continue
            
            if tmp == hash:
                output.append(L)
            
            # move the slide window left side boundgary, and pop element from tmp
            charL = s[L]
            tmp[charL] -= 1
            if tmp[charL] <= 0 :
                del tmp[charL] 
            L+=1

        return output

S = Solution()

print(S.findAnagrams("cbaebabaab", "aab"))