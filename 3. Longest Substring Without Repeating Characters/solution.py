import collections
from typing import List

from tomlkit import item

class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        if len(s) == 0: return 0
        d = collections.defaultdict(int)
        start = 0
        maxLen = 0
        
        l = r = 0

        while r < len(s) and l <= r:
            d[c] += 1
                        


        for i,c in enumerate(s) : 

            if c in d and d[c] >= start:
                start  = d[c]+1
            else:
                maxLen = max(maxLen, i-start+1)

            d[c] = i
        return maxLen
       

s=Solution()
print(s.lengthOfLongestSubstring("pwwkew"))

print(s.lengthOfLongestSubstring("mtmaunt"))

print(s.lengthOfLongestSubstring("abcabcbb"))

print(s.lengthOfLongestSubstring("bbbb"))

print(s.lengthOfLongestSubstring("dvdf"))
