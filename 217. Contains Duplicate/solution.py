from curses.ascii import SO
from typing import List

class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        d = {}
        for n in nums:
            if n in d:
                return True
            d[n] = 1
        return False

s=Solution()
r=s.containsDuplicate([1,1,1,3,3,4,3,2,4,2])
print(r)