
import collections
from typing import List,Optional
import heapq

class Codec:
    def encode(self, strs: List[str]) -> str:
        """Encodes a list of strings to a single string.
        """
        output = []
        for j in range(len(strs)):
            val = ''
            s = strs[j]
            for i in range(len(s)):
                c = ord(s[i])
                val += str(c)+ ('' if i == len(s) - 1  else "#")
            output.append(val)
        return '|'.join(output)

    def decode(self, s: str) -> List[str]:
        """Decodes a single string to a list of strings.
        """
        if len(s) == 0:
            return ['']
        output = []
        for arr in s.split('|'):
            strs = ''
            nums = ''
            for i in range(len(arr)):
                c = arr[i]
                if c.isdigit():
                    nums+=c

                if c == "#" or i == len(arr)-1:
                    strs += chr(int(nums))
                    nums = ''
            output.append(strs)
                
                

        return output
        
            
             
s=Codec()
e = s.encode(["", "s"])
d = s.decode(e)
print(e, d)


