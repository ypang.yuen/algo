import collections
from typing import List

class Solution:
    def combinationSum2(self, candidates: List[int], target: int) -> List[List[int]]:
        candidates.sort()
        output = []
        def backtrack(n, start, arr):
            if n == target:
                output.append(list(arr))
                return
            if n > target:
                return
            for i in range(start, len(candidates)):
                # compare last one to avoid duplicate
                if (i != start and candidates[i] == candidates[i-1]):
                    continue
                arr.append(candidates[i])
                backtrack(candidates[i]+n, i+1, arr)
                arr.pop()
        backtrack(0, 0 , [])
        return output

S = Solution()

print(S.combinationSum2([10,1,2,7,6,1,5], 8))

