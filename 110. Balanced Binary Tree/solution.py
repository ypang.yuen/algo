from pickle import FALSE
from typing import Optional
import collections
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution:
     def isBalanced(self, root: Optional[TreeNode]) -> bool:
        def dfs(root:Optional[TreeNode], h):
            if not root:
                return (True, 0)
            h+=1
            isLBal, left = dfs(root.left, h)
            isRBal, right = dfs(root.right, h)
            return abs(left - right) <= 1 and isLBal and isRBal, max(left,right) + 1

        isLBa, d = dfs(root, 0)   
        return isLBa
s=Solution()
n=TreeNode(1, 
    TreeNode(2, TreeNode(3, TreeNode(4)), TreeNode(3)), 
    TreeNode(2, None, TreeNode(3))  
)
r=s.isBalanced(n)
print(r)
n=TreeNode(1, 
    TreeNode(2, TreeNode(3, TreeNode(4, TreeNode(3)))), 
    TreeNode(2, None, TreeNode(3,  None, TreeNode(4, None, TreeNode(3))))
)
r=s.isBalanced(n)
print(r)