
import math
from typing import  List,Optional
import collections

#O(n)
class Solution:
    def compress(self, chars: List[str]) -> int:
        n = len(chars)
        if n == 1: return 1
        L = R = 0
        lastChar = chars[0]
        count = 1
        for R in range(1, n):
            c = chars[R]

            if c == lastChar:
                count+=1

            if c != lastChar or R == n-1:
                isSingleLast = c != lastChar and R == n-1
                chars[L] = lastChar
                if count > 1:
                    for i in str(count):
                        L+=1
                        chars[L] = i
                L+=1
                lastChar = c
                count = 1
                
                if isSingleLast:
                    chars[L] = lastChar
                    L+=1
        return L
s = Solution()
a= ["a","b","b","b","b","b","b","b","b","b","b","b","b","c","c"]
a= ["a","a","b","b","c"]
print(s.compress(a))


