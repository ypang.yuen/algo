import collections
from typing import List

class Solution:
    def canConstruct(self, ransomNote: str, magazine: str) -> bool:
        if len(ransomNote) > len(magazine): return False
        d=collections.defaultdict(list)
        for c in magazine:
          d[c] = d.get(c, 0) + 1
        for c in ransomNote:  
          d[c] = d.get(c, 0) - 1
          if d[c] < 0:
            return False
        return True

s = Solution()
r=s.canConstruct('aa', 'abdca')
print(r)