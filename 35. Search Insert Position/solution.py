
import collections
from typing import List,Optional
from bisect import bisect_left

class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:
        L, R = 0, len(nums)-1

        if target > nums[-1]:
            return len(nums)
        if target < nums[0]:
            return 0
        distance = R
        index = 1
        while L<= R:
            M = (L+R)// 2
            mid = nums[M]
            if mid == target:
                return M
            if abs(mid-target) < distance :
                if mid-target < 0:
                    index = M +1
                else:
                    index = M
                distance = abs(mid-target)
            if mid > target:
                R = M-1
            else:
                L = M+1
        return index

# Your Solution object will be instantiated and called as such:
s = Solution()
print(s.searchInsert([1,3,5,6], 2))

