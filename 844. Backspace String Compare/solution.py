class Solution:
    def backspaceCompare(self, s: str, t: str) -> bool:
        if s == t: return True
        s1 = []
        for i in range(0, len(s)):
            c = s[i]
            if c == '#':
                if len(s1) > 0:
                    s1.pop()
            else:
                s1.append(c)
        s2 = []
        for i in range(0, len(t)):
            c = t[i]
            if c == '#':
                if len(s2) > 0:
                    s2.pop()
            else:
                s2.append(c)

        return s1 == s2



s=Solution()
r = s.backspaceCompare('a##c', '#a#c')
print(r)