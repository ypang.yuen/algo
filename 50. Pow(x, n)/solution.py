from typing import List,Optional
class Solution:
    def myPow(self, x: float, n: int) -> float:
      
        def pow(x,n):
            if n == 0: return 1
            if x == 0: return 0
            res = pow(x, n//2)
            res *= res
            return res if n%2 == 0 else res * x

        x = pow(x,abs(n))
        return 1/x if n < 0 else x
        


S = Solution()
print(S.myPow(2,10))

