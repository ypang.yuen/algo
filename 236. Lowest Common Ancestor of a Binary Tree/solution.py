
import collections
from typing import List
from heapq import heappush, heappop

class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
# BFS O(N)
class Solution:
    def lowestCommonAncestor(self, root: TreeNode, p: TreeNode, q: TreeNode) -> TreeNode:
        self.ans = None

        def dfs(t):
            # base case if none, it is not match p or q
            if not t:
                return False
          
            left = dfs(t.left)
            
            right = dfs(t.right)
            
            isValid =  t.val == p.val or t.val == q.val

            # isValid repersent that the node itself match p or q
            # left repersent that left subtree has match p or q 
            # right repersent that right subtree has match p or q
            # since the node itself can be one of the LCA, hence either two is true, then this is the ans
            
            if isValid + left + right >= 2:
                self.ans = t

            return isValid or left or right

        dfs(root)

        print(self.ans)
        


s=Solution()

t = TreeNode(3)
t.left = TreeNode(5)
t.left.left = TreeNode(6)
t.left.right = TreeNode(2)

t.left.right.left = TreeNode(7)
t.left.right.right = TreeNode(4)

t.right = TreeNode(1)
t.right.left = TreeNode(0)
t.right.right = TreeNode(8)

print(s.lowestCommonAncestor(t, TreeNode(5), TreeNode(4) ))




