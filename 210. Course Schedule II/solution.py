class Solution:
    def findOrder(self, n: int, prerequisites: List[List[int]]) -> List[int]:
        adj = {i:[] for i in range(n)}

        for p in prerequisites:
            c, r = p
            adj[c].append(r)
        cycle = {}
        visit = {}
        order = []
        def dfs(c):
            if c in cycle:
                return False
            if c in visit:
                return True
            cycle[c] = 1
            for a in adj[c]:
                if not dfs(a):
                    return False
            del cycle[c]
            visit[c] = 1
            order.append(c)
            return True

        for p in range(n):
            if not dfs(p):
                return []
        return order