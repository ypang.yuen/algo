
import collections
from typing import List,Optional

class Solution:
    def reverseWords(self, s: List[str]) -> None:
        def swap(i,j):
            temp = s[i]
            s[i] = s[j]
            s[j] = temp
        def reverse(L,R):
            while L < R:
                swap(L,R)
                L+=1
                R-=1
        # reverse the whole list
        reverse(0, len(s)-1)

        # on each word, reverse back
        L=0
        for i in range(len(s)):
            if s[i] == ' ':
               reverse(L,i-1)
               L=i+1 
        reverse(L,i)
        
S=Solution()
S.reverseWords(["t","h","e"," ","s","k","y"," ","i","s"," ","b","l","u","e"])
