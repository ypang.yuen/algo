
from ast import Lambda
import collections
from typing import List,Optional
import heapq
# Back Tracking
class Solution:
    def canFinish(self, n: int, prerequisites: List[List[int]]) -> bool:
        adj = {i:[] for i in range(n)}
        
        for p in prerequisites:
            adj[p[0]].append(p[1])
            
        visit = set()
        cycle = set()
        output = []
        def dfs(c):
            if c in cycle:
                return False
            if c in visit:
                return True
            cycle.add(c)
            visit.add(c)
            for a in adj[c]:
                if not dfs(a):
                    return False
            cycle.remove(c)
            output.append(c)
            return True
        
        for i in range(n):
            if not dfs(i):
                return False
        
        # for course schedule 2
        # return output 
        return True

s=Solution()
n= 5
a =[[0,10],[3,18],[5,6],[7,5],[5,7],[6,11],[11,14],[13,1],[15,1],[17,4]]

print(s.canFinish(n,a))


             


