from typing import Optional
import collections
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def hasCycle(self, head: Optional[ListNode]) -> bool:
        fast = head
        while fast and fast.next:
            head = head.next
            fast = fast.next.next
            if head == fast: return True
        return False    

s=Solution()
n=ListNode(2)
n.next = ListNode(2)
n.next.next = ListNode(2)
n.next.next.next = ListNode(2)
n.next.next.next.next = ListNode(2)
r=s.hasCycle(n)
print(r)