from os import rename
from tokenize import cookie_re
from typing import List, Optional
from collections import Counter, deque
import heapq


   
class Solution:
    def leastInterval(self, tasks: List[str], n: int) -> int:        
        count = Counter(tasks)
        maxHeap = [-cnt for cnt in count.values()]
        heapq.heapify(maxHeap)
        time = 0
        q = deque() 
        while maxHeap or q:
            time += 1
            if not maxHeap:
                time = q[0][1]
            else:
                cnt = 1 + heapq.heappop(maxHeap)
                if cnt:
                    q.append([cnt, time + n])
            if q and q[0][1] == time:
                heapq.heappush(maxHeap,  q.popleft()[0])
        return time
s=Solution()




print(s.leastInterval(["A","A","A","B","B","B","C","C","C","D","D"], 2))
