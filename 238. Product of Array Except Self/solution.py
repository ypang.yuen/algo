# O(n) solution to product array with 2 pass
class Solution:
        # @param {integer[]} nums
        # @return {integer[]}
        def productExceptSelf(self, nums):
            p = 1
            n = len(nums)
            output = []
            ## find prefix
            for i in range(0,n):
                output.append(p)
                p = p * nums[i]
            p = 1
            ## find posfix and multply with pre
            for i in range(n-1,-1,-1):
                output[i] = output[i] * p
                p = p * nums[i]
            return output      
s=Solution()
print(s.productExceptSelf([1,2,3,4]))