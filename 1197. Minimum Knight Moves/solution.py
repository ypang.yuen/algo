
import collections
from typing import List,Optional
from bisect import bisect_left
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Node:
    def __init__(self, val = 0, neighbors = None):
        self.val = val
        self.neighbors = neighbors if neighbors is not None else []

class Solution:
    def minKnightMoves(self, x: int, y: int) -> int:
        
        q = collections.deque([(0,0)])
        
        hash = {}
        lv = 0
        while q:
            lv += 1
            for i in range(len(q)):
                _x,_y = q.popleft()
                
                if _x == x and _y == y:
                    return lv-1

                for X,Y in [ (_x+1, _y+2), (_x+2, _y+1), (_x+1, _y-2), (_x+2, _y-1), (_x-1, _y+2), (_x-2, _y+1), (_x-1, _y-2), (_x-2, _y-1)] :
                    if -300 <= X <= 300 and -300 <= Y <= 300 and X+Y <= 300 and (X,Y) not in hash:
                        # since we looping from the len of q, therefore the cache action mush happend before push to q
                        hash[(X,Y)] = 1
                        q.append((X,Y))

S = Solution()
x = 2
y = 112
print(S.minKnightMoves(x,y))