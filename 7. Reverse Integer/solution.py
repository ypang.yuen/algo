
import collections
from typing import List,Optional
import heapq
class Solution:
    def reverse(self, x: int) -> int:
        res = 0
        rev = abs(x)
        maxN = 2**31
        while rev > 0:
            res = res*10 + rev%10
            if not(res < maxN):
                return 0
            rev = rev//10
        if x < 0 : 
            res *= -1
        return res
S = Solution()
print(S.reverse(12345678923))

