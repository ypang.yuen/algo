
import collections
from ctypes.wintypes import tagMSG
from typing import List,Optional
from bisect import bisect_left, bisect_right
import heapq
from itertools import combinations
class Solution:
    def subArrayRanges(self, A):
        res = 0
        n = len(A)
        for i in range(n):
            l,r = A[i],A[i]
            for j in range(i, n):
                l = min(l, A[j])
                r = max(r, A[j])
                res += r - l
        return res
# Your Solution object will be instantiated and called as such:
s = Solution()
print(s.subArrayRanges(
[4,-2,-3,4,1],
))

