

from typing import  List,Optional
import collections
#O(n)
class Solution:
    def tictactoe(self, moves: List[List[int]]) -> str:
        n = 3
        row = [0] * n
        col = [0] * n
        ad = d = 0
        player = "A"

        
        for i in range(len(moves)):
            r , c = moves[i]
            if i%2 == 1:
                player = "B"
                row[r] -= 1
                col[c] -= 1
                if r + c == n-1:
                    ad -= 1
                if r - c == 0:
                    d -= 1
            else:
                player = "A"
                row[r] +=1
                col[c] += 1
                if r + c == n-1:
                    ad += 1
                if r - c == 0:
                    d += 1

            if abs(row[r]) == n or  abs(col[c]) == n or abs(ad) == n or abs(d) == n:
                return player
        
        if len(moves) < n*n:
            return "Pending"
        return "Draw"
        
            


s = Solution()
print(s.tictactoe([[0,0],[1,1],[2,0],[1,0],[1,2],[2,1],[0,1],[0,2],[2,2]]))


