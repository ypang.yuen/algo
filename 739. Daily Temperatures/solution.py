
import collections
from heapq import merge

from typing import List,Optional

# Stack solution, Runtime O(n*n), space O(n)
class Solution:
    def dailyTemperatures(self, temperatures: List[int]) -> List[int]:
        # init a stack for compare, !!!storing index into stack
        hash = []
        hash.append(0)
        # init the ans output, given that if nth to compare default is 0
        output = [0] * len(temperatures)
        
        for i in range(1, len(temperatures)):
            t = temperatures[i]
            # if current temp is > than the last temp in stack, then pop it out and compute the ans
            # e.g 
            # Current is 75, last is 74, 74 > 74 , pop 74 and compute the distance 
            # Stack [75]
            # Current cursor is 71, last is 75, 71 > 75, Skip
            # Stack [75,71]
            # Current is 69, last is 71, 69 > 71, Skip
            # Stack  [75,71,69]
            # Current is 72, last is 69, 72 > 69, pop 69 and compute the distance 
            # Stack [75,71]
            # 72 > 71, pop 71 and compute the distance 
            # Stack [75]
            # Stack [75,72]
            # ....
            while hash and temperatures[hash[-1]] < t:
                tempIndex = hash.pop()
                output[tempIndex] = i - tempIndex
            hash.append(i)
        return output
S = Solution()

print(S.dailyTemperatures([73,74,75,71,69,72,76,73]))