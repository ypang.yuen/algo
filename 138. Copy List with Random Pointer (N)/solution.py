from typing import Optional
class Node:
    def __init__(self, x: int, next: 'Node' = None, random: 'Node' = None):
        self.val = int(x)
        self.next = next
        self.random = random

class Solution:
    def copyRandomList(self, head: Optional[Node]) -> Optional[Node]:
        d = {None:None}
        cur = head
        while cur:
            copy = Node(cur.val)
            d[cur] = copy
            cur = cur.next

        cur = head

        while cur:
            copy = d[cur]
            copy.next = d[cur.next]
            copy.random = d[cur.random]
            cur = cur.next
            
        return d[head]


s=Solution()
n=Node(1)
n2=Node(2)
n3=Node(3)
n4=Node(4)
n5=Node(5)
n6=Node(6)
n7=Node(7)

n.next = n2
n2.next = n3 
n3.next = n4 
n4.next = n5 
n5.next = n6 
n6.next = n7 
n7.next = None

n.random=  n4
n2.random = None
n3.random = n5
n4.random = n2
n5.random = n7
n6.random = n
n7.random = n2

print(s.copyRandomList(n))

