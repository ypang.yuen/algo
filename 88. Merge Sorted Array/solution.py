from platform import node
from turtle import done, end_fill
from typing import List, Optional
import collections

# easy way is combine and sort
# med way is copy nums1 , and use the copy to compare and assign to nums1
# hardest way is 3 pointer solution

# P1 point to end of m (m-1)
# p2 point to end of n (n-1)
# p3 start from num1 end
# keep move p3
# if p2 is used all, job done
# if p1 >= p2 , assign val and <- p1 pointer  
# else assign val and <- p2 pointer  

class Solution:
    def merge(self, nums1: List[int], m: int, nums2: List[int], n: int) -> None:
        """
        Do not return anything, modify nums1 in-place instead.
        """
        p1 = m-1
        p2 = n-1
        if n > 0:
            for i in range(len(nums1)-1, -1, -1):
                if p2 == 0:
                    break
                if nums1[p1] >= nums2[p2] and p1 >=0 :
                    nums1[i] = nums1[p1]
                    p1-=1
                else:
                    nums1[i] = nums2[p2]
                    p2-=1
