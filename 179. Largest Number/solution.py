class Compare(str):
    def __lt__(x,y):
        return x+y > y+x
class Solution:
    def largestNumber(self, nums: List[int]) -> str:
        res = sorted(map(str, nums), key=Compare)
        strRes = ''.join(res)
        return '0' if strRes[0] == '0' else strRes