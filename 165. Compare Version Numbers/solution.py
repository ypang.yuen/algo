from typing import List

class Solution:
    def compareVersion(self, version1: str, version2: str) -> int:
        v1 = version1.split('.')
        v2 = version2.split('.')
        v = v1 if len(v1) > len(v2) else v2
        arr = []
        for i in range(len(v)):
            val1 = int(v1[i]) if i < len(v1) else 0
            val2 = int(v2[i]) if i < len(v2) else 0
            if val1 < val2 : return -1
            if val1 > val2 : return 1
        return 0

s=Solution()

a = "1.01"
b = "1.001"
print(s.compareVersion(a, b))