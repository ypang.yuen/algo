import collections
from typing import List

#space O(26)
#run O(M) + O(N)
class Solution:
    def nextGreaterElement(self, n: int) -> int:
        data = list(str(n))
        length = len(data)

        def swap(L,R):
            T = data[L]
            data[L]= data[R]
            data[R] = T
        def reverse(L):
            R = length
            while L<R:
                swap(L,R)
                L+=1
                R-=1


        for L in range(length-1, -1, -1):
            if L != length-1 and data[L] < data[L+1]: break

        for R in range(length-1, -1, -1):
            if data[R] > data[L]: break
        
        if R > 0:
            swap(L,R)
            L+=1
        reverse(L)
        
        maxVal = 2**31-1
        ans = int(''.join(data))
        return -1 if ans > maxVal else ans

       

s=Solution()
print(s.nextGreaterElement(123321))
