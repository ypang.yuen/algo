
import collections
from typing import List,Optional

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:
    def zigzagLevelOrder(self, root: Optional[TreeNode]) -> List[List[int]]:
        if not root : return []
        q = collections.deque([root])
        L2R = True
        output = []
        while q:
            arr = collections.deque([])
            for i in range(len(q)):
                c = q.popleft()
                if L2R:
                    arr.append(c.val)
                else:
                    arr.appendleft(c.val)
                    
                if c.left:
                    q.appendleft(c.left) 
                if c.right:
                    q.appendleft(c.right) 
                    
            output.append(list(arr))

            L2R = True if L2R == False else False
        
        return output

S=Solution()
t = TreeNode(10, TreeNode(5), TreeNode(-3))

print(S.zigzagLevelOrder(t))

