import collections
from typing import List

class Solution:
    def permute(self, nums: List[int]) -> List[List[int]]:
        output = []
        d = collections.Counter(nums)
        def backtrack(arr):
            if len(arr) == len(nums):
                output.append(list(arr))
                return
            for n in d:
                if d[n] == 0:
                    continue
                arr.append(n)
                d[n]-=1
                backtrack(arr)
                d[n]+=1
                arr.pop()
        backtrack([])
        return output

S = Solution()

print(S.permute([1,2,3]))

