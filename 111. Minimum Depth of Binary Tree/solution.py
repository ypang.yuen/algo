from os import rename
from tokenize import cookie_re
from typing import List, Optional
from collections import Counter, deque
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution:
    def minDepth(self, root: Optional[TreeNode]) -> int:
        if not root: return 0
        q = deque([(root, 1)])
        
        while q:
            c, lv = q.popleft()
            if not c.left and not c.right:
                return lv
            if c.left:
                q.append((c.left, lv+1))
            
            if c.right:
                q.append((c.right, lv+1))
