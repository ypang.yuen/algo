import collections

class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        if len(s) != len(t): return False
        a=list(s)
        b=list(t)
        for i in range(len(s)):
            s1=s[i]
            s2=t[i]
            if s1 in b: b.remove(s1)
            if s2 in a: a.remove(s2)
        return len(a) + len(b)  == 0

class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        if len(s) != len(t): return False
        chars = collections.defaultdict(int)
        for c in s:
            chars[c] += 1

        for c in t:
            chars[c] -= 1
            if chars[c] == -1: return False

        return True

s=Solution()
n='aba'
n1='bba'
r=s.isAnagram(n,n1)
print(r)


