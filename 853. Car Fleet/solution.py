
import collections
from typing import List,Optional

class Solution:
    def carFleet(self, target: int, position: List[int], speed: List[int]) -> int:
        pair = [[p, s] for p, s in zip(position, speed)]
        crt = last = 0
        pair.sort(reverse=True)
        for p, s in pair:
            sp = (target - p) / s
            if sp > last:
                last = sp
                crt +=1
        return crt
                
        
s = Solution()
s.carFleet(12, [10,8,0,5,3], [2,4,1,1,3])
