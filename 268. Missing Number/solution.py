from typing import List
class Solution:
    def missingNumber(self, nums: List[int]) -> int:
        total = len(nums)
        gTotal=(total*(total+1))//2
        for n in nums:
            gTotal-=n
        return gTotal

# it will be better if using xor to avoid stackoverflow
class Solution2:
    def missingNumber(self, nums: List[int]) -> int:
        n = 0
        for i in range(len(nums)):
            n = n ^ nums[i] ^ (i+1)
        return n


s=Solution2()
print(s.missingNumber( [1,3,4,5,6,7,0]))