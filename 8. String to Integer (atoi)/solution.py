
from ast import Lambda
import collections
from typing import List,Optional
import heapq
class Solution:
    def myAtoi(self, s: str) -> int:
        res = 0
        prev = ''
        for i in range(len(s)):
            if s[i].isalpha() or s[i] == '.':
                return res
            if (prev == '-' or prev == '+' or s[i].isdigit()):
                j = i
                while j < len(s) and s[j].isdigit():
                    res *= 10
                    res += int(s[j])
                    j+=1
                break
            prev = s[i]
        
        factor = -1 if prev == '-' else 1
        res*=factor

        minRange = -2 ** 31
        maxRange = (2 ** 31) -1
        if minRange <= res <= maxRange:
            return res
        return maxRange if factor > 0 else minRange
s=Solution()

print(s.myAtoi("-91283472332"))


             


