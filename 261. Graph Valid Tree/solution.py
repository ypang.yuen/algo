
from ast import Lambda
import collections
from typing import List,Optional
import heapq
class Solution:
    def validTree(self, n: int, edges: List[List[int]]) -> bool:
        if not n :
            return True
        
        hash = {}
        for i in range(len(edges)):
            start, end = edges[i]
            if start not in hash:
                hash[start] = []
            if end not in hash:
                hash[end] = []
            hash[start].append(end)
            hash[end].append(start)


        def dfs(node, prev):
            if node in seen:
                return False
            seen[node] = 1
            for n in hash[node]:
                if n == prev:
                    continue
                if not dfs(n, node):
                    return False
            return True
        seen = {}
        start , end = edges[0]
        valid = dfs(start, -1)
        return valid and len(seen) == n




s=Solution()
n= 3
a =[[1,0],[2,0]]



print(s.validTree(n,a))


             


