
import collections
from typing import List,Optional
import heapq

# O(N) runtime
class HitCounter:

    def __init__(self):
        self.stack = []

    def hit(self, timestamp: int) -> None:
        self.stack.append(timestamp)

    def getHits(self, timestamp: int) -> int:
        start = timestamp - 300
        count = 0
        for i in self.stack:
            if i > start and i <= timestamp:
                count += 1
        return count

# O(300) => O(1) runtime, consistant 
class HitCounter2:

    def __init__(self):
        self.stack = collections.deque([])

    def hit(self, timestamp: int) -> None:
        self.stack.append(timestamp)

    def getHits(self, timestamp: int) -> int:
        start = timestamp - 300
        while self.stack:
            if self.stack[0] <= start:
                 self.stack.popleft()
            else:
                break

        return len(self.stack)

# Your HitCounter object will be instantiated and called as such:
obj = HitCounter()

obj.hit(1)
obj.hit(2)
obj.hit(3)
obj.hit(4)
obj.hit(5)
obj.hit(300)

print(obj.getHits(301))
print(obj.getHits(302))

