import collections
from typing import List, Optional
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution:
    def sortedArrayToBST(self, nums: List[int]) -> Optional[TreeNode]:
        if not nums: return None

        mid = len(nums) // 2
        node = TreeNode(nums[mid])

        node.right = self.sortedArrayToBST(nums[mid+1::])
        node.left = self.sortedArrayToBST(nums[0:mid])

        return node




s = Solution()
r = s.sortedArrayToBST([1,2,3,4,5,6,7])


stack = collections.deque([r])

while stack:
    c = stack.popleft()
    print(c.val)
    if(c.right): stack.append(c.right) 
    if(c.left): stack.append(c.left) 
