from ctypes.wintypes import tagRECT
from turtle import right
from typing import List


class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        ans = []
        l = len(nums)
        
        nums.sort()
        
        for i in range(l):
            if i > 0 and nums[i] == nums[i-1]: continue
            n = nums[i]
            
            L = i+1
            R = l-1
            while L < R:
                total = n + nums[L] + nums[R]
                if total == 0:
                    ans.append([n , nums[L] , nums[R]])
                    while L<R:
                        if nums[L] == nums[L+1]:
                            L+=1
                        else:
                            break
                
                if total > 0:
                    R -= 1
                else:
                    L +=1
                    
        return ans

s=Solution()
print(s.threeSum([-1,0,1,2,-1,-4]))
