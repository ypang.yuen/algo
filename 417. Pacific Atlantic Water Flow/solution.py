
import collections
from heapq import merge

from regex import B

from typing import List,Optional
#DFS
class Solution:
    def pacificAtlantic(self, heights: List[List[int]]) -> List[List[int]]:
        w, h  = len(heights[0]), len(heights)
        
        def dfs2(y,x, hash):
            cur = heights[y][x]
            hash.add((y,x))
            for Y,X in [(y+1,x),(y-1,x),(y,x+1),(y,x-1)]:
                if 0 <= Y < h and 0 <= X < w and  heights[Y][X] <= cur and (Y,X) not in hash:
                    dfs(Y,X, hash)


        def dfs(row, col, reachable):
            # This cell is reachable, so mark it
            reachable.add((row, col))
            for (x, y) in [(1, 0), (0, 1), (-1, 0), (0, -1)]: # Check all 4 directions
                new_row, new_col = row + x, col + y

                if new_row < 0 or new_row >= h or new_col < 0 or new_col >= w:
                    continue
                if (new_row, new_col) in reachable:
                    continue
                if heights[new_row][new_col] <  heights[row][col] :
                    continue
                dfs(new_row, new_col, reachable)     

        hashPacific = set()
        hashAtlantic = set()
        for i in range(h):
            dfs(i, 0, hashPacific)
            dfs(i, w - 1, hashAtlantic)
        for i in range(w):
            dfs(0, i, hashPacific)
            dfs(h - 1, i, hashAtlantic)

        return list(hashPacific.intersection(hashAtlantic))

S = Solution()
n = [[1,2,2,3,5],[3,2,3,4,4],[2,4,5,3,1],[6,7,1,4,5],[5,1,1,2,4]]
print(S.pacificAtlantic(n))

#BFS
# add all start point from pacific and atlantic, which are the 4 side edge of the island
# scan 2 times and find the intescation 
class Solution:
    def pacificAtlantic(self, heights: List[List[int]]) -> List[List[int]]:
        R, C = len(heights), len(heights[0])
        pacific = collections.deque([])
        atlantic = collections.deque([])
        pacificHash = set()
        atlanticHash = set()
        for i in range(C):
            pacific.append([0,i])
            atlantic.append((R-1, i))
        for i in range(R):
            pacific.append((i,0))
            atlantic.append((i, C-1))
       
        def bfs(queue, hash):
            while queue:
                r,c = queue.pop()
                hash.add((r,c))
                for _R,_C in [(r,c+1),(r,c-1),(r+1,c),(r-1,c)] :
                    # water fall from higher r,c, hence, the new r,c must larger or equal to the current r,c
                    if 0 <= _R < R and 0 <= _C < C and heights[_R][_C] >= heights[r][c] and  (_R,_C) not in hash:
                        queue.append((_R,_C))
       
        bfs(pacific, pacificHash)
        bfs(atlantic, atlanticHash)
        return list(pacificHash.intersection(atlanticHash))