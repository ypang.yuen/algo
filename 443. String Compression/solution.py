
from typing import List,Optional
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution:
    def inorderSuccessor(self, root: TreeNode, p: TreeNode) -> Optional[TreeNode]:
        self.find = False
        self.res = None
        def dfs(root):
            if root.left:
                if dfs(root.left, root):
                    return True
            print(root.val)
            if self.find:
                if root.val > p.val:
                    self.res = root
                    return True
                return False

            if root.val == p.val:
                self.find = True
                
            if root.right:
                if dfs(root.right, root):
                    return True

            return False
        
        dfs(root)
        return self.res 
S=Solution()
n=TreeNode(5, TreeNode(3, TreeNode(2, TreeNode(1)), TreeNode(4)),TreeNode(6))
t= TreeNode(9, n , TreeNode(10))

print(S.inorderSuccessor(t, TreeNode(6)))