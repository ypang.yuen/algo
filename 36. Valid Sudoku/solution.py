
import collections
from heapq import merge

from typing import List,Optional

# we wanna check the sudoku is valid , by checking the val has repeated in row , col and 3x3 square
# therefore we need 3 collection hash set to compare
# store all val in each row into diff hash set, as same as col
# therefore, when we traversal to the same row or same col, we can check where current val has existed in same row or col
# Tricy part is, for 3x3 square, we  need to use number division i // 3 to categoize the val
class Solution:
    def isValidSudoku(self, board: List[List[str]]) -> bool:
        rowHash = collections.defaultdict(set)
        colHash = collections.defaultdict(set)
        squareHash = collections.defaultdict(set)

        cols, rows = len(board[0]), len(board)

        for y in range(rows):
            for x in range(cols):
                val = board[y][x]
                if val == ".":
                    continue
                if val in rowHash[y] or val in colHash[x] or val in squareHash[(y//3, x//3)]:
                    return False

                rowHash[y].add(val)
                colHash[x].add(val)
                squareHash[(y//3, x//3)].add(val)

        return True

S = Solution()
board = [["5","3",".",".","7",".",".",".","."]
,["6",".",".","1","9","5",".",".","."]
,[".","9","8",".",".",".",".","6","."]
,["8",".",".",".","6",".",".",".","3"]
,["4",".",".","8",".","3",".",".","1"]
,["7",".",".",".","2",".",".",".","6"]
,[".","6",".",".",".",".","2","8","."]
,[".",".",".","4","1","9",".",".","5"]
,[".",".",".",".","8",".",".","7","9"]]
print(S.isValidSudoku(board))

