
import collections
from typing import List,Optional
import heapq
import random
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
        self.parent = None


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
class RandomizedSet:

    def __init__(self):
        self.hash = {}
        self.list = []

    def insert(self, val: int) -> bool:
        if val not in self.hash:
            self.hash[val] = len(self.list)
            self.list.append(val)
            return True
        return False

    def remove(self, val: int) -> bool:
        if val in self.hash:
            currInx = self.hash[val]
            last = self.list[-1]
            self.list[currInx] = last 
            self.hash[last] = currInx
            self.list.pop()
            del self.hash[val]
            return True
        return False

    def getRandom(self) -> int:
        return random.choice(self.list)
    

s=RandomizedSet()
prev = ListNode(0)
cur = prev
for i in range(1,10):
    s.insert(i)

s.remove(5)
s.remove(7)

print(s.getRandom())
 # while head:
            
        #     head = head.next
             


