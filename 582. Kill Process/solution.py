

from typing import  List,Optional
import collections
#O(n)
class Solution:
    def killProcess(self, pid: List[int], ppid: List[int], kill: int) -> List[int]:
        adj = {}

        for i in range(len(pid)):
            child = pid[i]
            parent = ppid[i]
            if parent not in adj:
                adj[parent] = []
            adj[parent].append(child)

        output = []
        def dfs(node):
            output.append(node)
            if node not in adj:
                return
            for i in adj[node]:
                dfs(i)

       
        dfs(kill)

        return output
s = Solution()
a= [1,3,10,5]
b= [3,0,5,3]
c = 5

print(s.killProcess(a,b,c))


