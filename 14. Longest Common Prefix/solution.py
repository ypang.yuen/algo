from typing import List

class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        if len(strs) == 1: return strs[0]
        s = strs[0]
        prefix = ''
        b = True
        for j in range(0, len(s)):
            c = s[j]
            for i in range(1, len(strs)):
                s2 = strs[i]
                if j >= len(s2) or c != s2[j]:
                    b = False
                    break
            if b: 
                prefix += c

        return prefix
s=Solution()
r = s.longestCommonPrefix(["flower","flow","flight"])
print(r)