
import collections
from ctypes.wintypes import tagMSG
from typing import List,Optional

from regex import F
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution:
    def twoSumBSTs(self, root1: Optional[TreeNode], root2: Optional[TreeNode], target: int) -> bool:
        q1 = collections.deque([root1])
        remain = set()
        while q1:
            node = q1.popleft()
            remain.add(target - node.val)
            if node.left:
                q1.append(node.left)
            if node.right:
                q1.append(node.right)
        q1 = collections.deque([root2])
        while q1:
            node = q1.popleft()
            if node.val in remain:
                return True
            if node.left:
                q1.append(node.left)
            if node.right:
                q1.append(node.right)
        return False
# Your Solution object will be instantiated and called as such:
s = Solution()
print(s.suggestedProducts(
["mobile","mouse","moneypot","monitor","mousepad"],
"mouse"
))
