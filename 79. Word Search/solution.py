
import collections
from heapq import merge

from typing import List,Optional
#
class Solution:
    def exist(self, board: List[List[str]], word: str) -> bool:
        w,h = len(board[0]), len(board)
       
        def dfs(y,x,index):
            if len(word) == index:
                return True
            if not( 0 <= y < h and 0 <= x < w ) or word[index] != board[y][x]: 
                return False
            
            board[y][x] = '#'
            isFound = False
            for X,Y in [(x+1,y),(x-1,y),(x,y+1),(x,y-1)]:    
                    isFound = dfs(Y,X, index+1)
                    if isFound: break
                        
            board[y][x] = word[index]
            
            return isFound

        for y in range(0, h):
            for x in range(0, w):
                    found = dfs(y,x, 0)
                    if found: return found
       
        return False

S = Solution()
print(S.exist([["A","A","A","A","A","A"],["A","A","A","A","A","A"],["A","A","A","A","A","A"],["A","A","A","A","A","A"],["A","A","A","A","A","B"],["A","A","A","A","B","A"]],
"AAAAAAAAAAAAABB"))