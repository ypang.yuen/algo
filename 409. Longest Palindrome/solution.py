import collections
from typing import List

class Solution1:
    def longestPalindrome(self, s: str) -> int:
        d = collections.defaultdict(int)
        for x in s:
            d[x]+=1
        r = 0
        ex = 0
        for x in d.values():
            r += x // 2 * 2
            if ex ==0 and x%2 == 1:
              ex = 1
        
        return r + ex

class Solution2:
    def longestPalindrome(self, s: str) -> int:
        d = collections.defaultdict(int)
        r = 0
        for c in s:
            if d.get(c):
                del d[c]
                r+=2
            else:
                d[c] = 1

        ex = 1 if len(d) > 0 else  0
        return r + ex

s = Solution2()
r=s.longestPalindrome('aaaaccddsslfhgg')
print(r)