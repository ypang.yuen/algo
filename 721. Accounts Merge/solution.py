
from typing import List

class Solution:
    def accountsMerge(self, accounts: List[List[str]]) -> List[List[str]]:
        adj = {}
        visit = {}
        output = []
        # build adj hash, where using the first email as key on each accounts, and the emails that neighbour with first email as adj, 
        # e.g { email : [adj emails] }
        for acc in accounts:
            e = acc[1]
            if e not in adj:
                adj[e] = []
            for i in range(2, len(acc)):
                email = acc[i]
                adj[e].append(email)

                # e.g since adj is bi-direction, we also need to build the adj for the current neighbour
                if email not in adj:
                    adj[email] = []

                # current neighbour will also connect to current email # e.g {a : [b,c], b: [a], c:[a]}
                adj[email].append(e)

        
        # A dfs function to build list, and find all neighbour of current email to build the list with the adj hash
        def dfs(acc, email):
            acc.append(email)
            visit[email] = 1
            
            if email in adj:
                for e in adj[email]:
                    if e not in visit:
                        dfs(acc, e)
            
        for acc in accounts:
            name = acc[0]
            email = acc[1]
            if email not in visit:
                mergeAcc = []
                dfs(mergeAcc, email)
                mergeAcc.sort()
                output.append([name] + mergeAcc)
        return output
        
s=Solution()
print(s.accountsMerge([["John","johnsmith@mail.com","john_newyork@mail.com"],["John","johnsmith@mail.com","john00@mail.com"],["Mary","mary@mail.com"],["John","johnnybravo@mail.com"]]
))


    
