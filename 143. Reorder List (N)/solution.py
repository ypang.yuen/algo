from typing import Optional
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
class Solution:
    def reorderList(self, head: Optional[ListNode]) -> None:
        slow = fast = head
        #e.g 1,2,3,4,5
        while fast and fast.next:
            fast = fast.next.next
            slow = slow.next
        #slow = 3,4,5
        
        
        second  = slow.next
        #second = 4,5, this is to remember the 4,5 , as we going to assign it to None

        rev = slow.next = None
        # #slow = 3
        # #head = 1,2,3 becoz slow is the cursor, if slow next is none, means we cut the head till 3
        # changing slow is not change head, changing slow.next is telling head.next is point to where
        
        # ## revert 4,5 => 5,4
        while second:
            second.next, rev, second = rev, second, second.next
           
        # ## first => 1,2,3, second = 5,4
        first, second = head, rev

        while second:
            first.next, second.next, first, second = second, first.next, first.next, second.next
    
        while head:
            print(head.val)
            head = head.next



s = Solution()
i = 4
n = ListNode(0)
while i >= 0:
    tmp = ListNode(i+1)
    
    tmp.next = n.next
    
    n.next = tmp
    
    i-=1

print(s.reorderList(n.next))