from typing import Optional, List
import collections
class Solution:
    def strStr(self, haystack: str, needle: str) -> int:
        if not needle: return 0
        L1 = len(needle)
        for i in range(L1-1, len(haystack)):
            cur = haystack[i]
            if cur == needle[-1]:
                isSame = True
                t = i
                for j in range(L1-1, -1, -1):
                    if needle[j] != haystack[t-L1+1+j]:
                        isSame = False
                        break
                if isSame:
                    return i-L1+1
        return -1



haystack = "helloasdaa"
needle = "sd"
s=Solution()
print(s.strStr(haystack, needle))
