from typing import List, Optional
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

# Compare each node's subtree to the given subtree, O(n^m) runtime, O(1) space

# Create a helper function same tree to compare between 2 trees

# for each node in the main tree, use the helper function to compare

class Solution:
    def sameTree(self, p: Optional[TreeNode], q: Optional[TreeNode]):
        if p is None and q is None: return True
        
        if p is None or q is None or p.val != q.val:
            return False
        
        return self.sameTree(p.left, q.left) and self.sameTree(p.right, q.right)
    
    def subTree(self, p: Optional[TreeNode], q: Optional[TreeNode]) -> bool:
        # if the given subtree is none, it is always true
        if q is None : return True

        # if the main tree is none, and given subtree not none, it is always false
        if p is None : return False

        # compare current node's subtree and the given subtree, if true, we can just return the ans
        if self.sameTree(p, q):
            return True

        # move to next child node
        return self.subTree(p.left, q) or self.subTree(p.right, q) 
                

        

s=Solution()
n=TreeNode(1, TreeNode(3, TreeNode(2,TreeNode(4), TreeNode(5))), TreeNode(3, TreeNode(6)))
n2=TreeNode(2, TreeNode(4), TreeNode(5))
r=s.subTree(n, n2)
print(r)

