import collections
from typing import List
class Solution:
    def longestConsecutive(self, nums: List[int]) -> int:
        longestSqu = 0
        d=set(nums)
        for n in d:
            if n-1 not in d:
                seq = 0
                while n+seq in d:
                    seq +=1
                longestSqu = max(longestSqu, seq)
                if longestSqu > len(nums)//2:
                    return longestSqu
        return longestSqu

        
s=Solution()

arr = [i for i in range(10000)]
print(arr)

print(s.longestConsecutive([100,1,102,103,2,4,5,3,0]))
