import re
import heapq
class Solution:
    def isPalindrome(self, s: str) -> bool:
          f= re.sub(r'[^a-zA-Z0-9]', '', s) 
          f = f.lower()
          l = len(f)
          r = l % 2
          for i in range(l):
               if r == 0 and l / 2 == (i+1):
                    return f[:i+1] == f[i+1:l][::-1]
               if r == 1 and (l-1) / 2 == (i+1):
                    return f[:i+1] == f[i+1+1:l][::-1]
          return True

class Solution2:
    def isPalindrome(self, s: str) -> bool:
          f= re.sub(r'[^a-zA-Z0-9]', '', s) 
          l = len(f)
          r = l % 2
          for i in range(l):
               h=f[i]
               t=f[-1-i]
               if h.lower() != t.lower():
                  return False
          return True

class Solution3:
    def isPalindrome(self, s):
          l, r = 0, len(s)-1
          while l < r:
               while l < r and not s[l].isalnum():
                    l += 1
               while l <r and not s[r].isalnum():
                    r -= 1
               if s[l].lower() != s[r].lower():
                    return False
               l +=1; r -= 1
          return True

s = Solution()
r=s.isPalindrome('A man, a plan, a canal: Panama')
print(r)

s = Solution2()
r=s.isPalindrome('A man, a plan, a canal: Panama')
print(r)


#https://leetcode.com/problems/valid-palindrome/