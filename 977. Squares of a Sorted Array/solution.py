import collections
from typing import List
class Solution:
    def sortedSquares(self, nums: List[int]) -> List[int]:
        left, right = 0 , len(nums) - 1
        res = collections.deque()
        while left <= right:
            l, r = abs(nums[left]), abs(nums[right])
            if l > r:
                res.appendleft(l * l)
                left += 1
            else:
                res.appendleft(r * r)
                right -= 1
        return res


s=Solution()
print(s.sortedSquares([-4,-1,0,4,10]))
#print(s.topKFrequent([1], 1))


