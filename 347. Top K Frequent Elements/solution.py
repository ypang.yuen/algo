import collections
from typing import List
class Solution:
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:
        if len(nums) == 1 : return nums
        d = collections.defaultdict(int)
        counter = [[]] * (len(nums)+1 )
        # find counter
        for n in nums:
            d[n] += 1

        for key,count in d.items():
            if len(counter[count]) == 0:
                counter[count] = [key]
            else:
                counter[count] += [key]

        res = []
        i = len(counter)-1
        while i >= 0:
            for item in counter[i]:
                if len(res) == k:
                    break
                res.append(item)
            i-=1
        return res

s=Solution()
print(s.topKFrequent([1], 1))
#print(s.topKFrequent([1], 1))


