from typing import Optional
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
class Solution:
    def isPalindrome(self, head: Optional[ListNode]) -> bool:
        rev = None
        slow = fast = head
        while fast and fast.next:
            fast = fast.next.next
            rev, rev.next, slow = slow, rev, slow.next
        
        # get half of reverised list

        if fast: 
            slow = slow.next # move foward if head length is odd
    
       # print("fast", slow.val,slow.next.val, slow.next.next)

        while rev:
            if rev.val != slow.val:
                return False
            rev = rev.next
            slow = slow.next
        return True
        

s=Solution()
n=ListNode(1)
n.next=ListNode(2)
n.next.next=ListNode(3)
n.next.next.next=ListNode(3)
n.next.next.next.next=ListNode(2)
n.next.next.next.next.next=ListNode(1)
print(s.isPalindrome(n))

