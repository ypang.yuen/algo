from typing import List
class Solution:
    def moveZeroes(self, nums: List[int]) -> None:
        index = 0
        for i in range(len(nums)):
           if nums[i] != 0:
               nums[i],nums[index] = nums[index],nums[i]
               index += 1
        print(nums)
  

s=Solution()
print(s.moveZeroes([0,1,0,3,12]))




