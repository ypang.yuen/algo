import collections
from typing import List

#space O(26)
#run O(M) + O(N)
class Solution:
    def minWindow(self, s: str, t: str) -> str:
        d1 = collections.defaultdict(int)
        d2 = {}
        left, ANSL, ANSR, minL = 0, -1,-1, len(s)
        
        for c in t:
            d1[c] += 1
        
        for i in range(len(s)):
            c = s[i]
            if c in d1:
                d2[c] = d2.get(c, 0) + 1

            found = True

            for k in d1:
                if k not in d2 or d2[k] < d1[k]:
                    found = False
                    break

            while found:
                if i-left-1 < minL:
                    ANSL = left
                    ANSR = i
                    minL = i-left-1
                newC = s[left]
                
                if newC in d2:
                    d2[newC] -= 1
                    if d2[newC] <= 0:
                        d2.pop(newC)
                
                left+=1
                
                for k in d1:
                    if k not in d2 or d2[k] < d1[k]:
                        found = False
                        break
            
        return s[ANSL:ANSR+1]
s=Solution()
#print(s.minWindow( "ADOBECODEBANC", "ABC"))
print(s.minWindow( "acabwefgewcwaefgcf", "cae"))
print(s.minWindow( "a", "aa"))
print(s.minWindow( "aa", "a"))
print(s.minWindow( "a", "a"))
#print(s.characterReplacement("ABABBA", 2))
# print(s.characterReplacement("AABABCBA", 3))
#print(s.characterReplacement("A",1))



