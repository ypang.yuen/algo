from xml.etree.ElementTree import XML


class Solution:
    def romanToInt(self, s: str) -> int:
        r = {
            'I':1,
            'V':5,
            'X':10,
            'L':50,
            'C':100,
            'D':500,
            'M':1000
        }
        cs = {
            'XL': 40,
            'XC': 90,
            'IV': 4,
            'IX': 9,
            'CD': 400,
            'CM': 900
        }
        total = 0
        i = 0
        while i < len(s) : 
            c = s[i]
            if i + 1 == len(s):
               total+=r[c]
               break
            nc = s[i+1]
            if (c+nc) in cs:
                total+=cs[c+nc]
                i+=1
            else:
                total+=r[c]
            i+=1
            print(i)
        return total


s=Solution()
r=s.romanToInt('MCMXCIVIIIVIVIV')
print(r)