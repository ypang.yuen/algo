# The knows API is already defined for you.
# return a bool, whether a knows b
def knows(a: int, b: int) -> bool:
    return True
class Solution:
    def findCelebrity(self, n: int) -> int:
        cache = {}
        celebrity = 0
        def cachedKnow(a,b):
            if (a,b) not in cache:
                res = knows(a,b)
                cache[(a,b)] = res
            return cache[(a,b)] 
        
        for i in range(n):
            if celebrity == i:
                continue
            if cachedKnow(celebrity, i):
                celebrity = i
                
        for i in range(n):
            if celebrity == i :
                continue
            if  not cachedKnow(celebrity, i) and cachedKnow(i, celebrity):
                continue
            return -1   
        return celebrity
                   
        