import collections
from typing import List

class Solution:
    def subsets(self, nums: List[int]) -> List[List[int]]:
        nums.sort()
        def backtrack(i = 0,   arr = []):
            output.append(list(arr))
            for s in range(i, len(nums)):
                if s != i and nums[s] == nums[s-1]:
                    continue
                arr.append(nums[s])
                backtrack(s+1, arr)
                arr.pop()
        output = []
        backtrack()
        return output


S = Solution()
print(S.subsets([1,3,2,2]))