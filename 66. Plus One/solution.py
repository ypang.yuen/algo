from typing import List

class Solution:
    def plusOne(self, digits: List[int]) -> List[int]:
        s = ''
        for n in digits:
            s = s + str(n)
        r = int(s)+1
        return list(str(r))

        

s = Solution()
r=s.plusOne([1,9,9,9])
print(r)
