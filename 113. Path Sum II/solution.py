
from typing import List,Optional

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def pathSum(self, root: Optional[TreeNode], targetSum: int) -> List[List[int]]:
        if not root:
            return []
        output = []
        def backTrack(root, arr = [], sum = 0):
            if sum == targetSum and not root.left and not root.right:
                output.append(list(arr))
                return

            if root.left:
                arr.append(root.left.val)
                backTrack(root.left, arr, sum+root.left.val)
                arr.pop()

            if root.right:
                arr.append(root.right.val)
                backTrack(root.right, arr, sum+root.right.val)
                arr.pop()

        backTrack(root, [root.val], root.val)

        print(output)
S=Solution()
t = TreeNode(
    -2,
    None,
    TreeNode(-3
    )
)

print(S.pathSum(t,-5))