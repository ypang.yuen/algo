
import collections
from ctypes.wintypes import tagMSG
from typing import List,Optional

class Solution:
    def snakesAndLadders(self, board: List[List[int]]) -> int:
        step = 0
        newBoard = []
        odd = False
        for n in board[::-1]:
            newBoard += (n if not odd else n[::-1])
            odd = False if odd else True
        
        start = newBoard[0]-1 if newBoard[0] > -1 else 0

        q = collections.deque([start])

        target = len(newBoard)

        seen = set()
        
        while q:
            for j in range(len(q)):
                pos = q.popleft()
                if pos >= target-1:
                    return step
                for i in range(pos+1, min(pos+7, target)):
                    dest =  newBoard[i]-1 if newBoard[i] > -1 else i
                    if dest in seen:
                        continue
                    seen.add(dest)
                    q.append(dest)
            step+=1
        return -1
        
# Your Solution object will be instantiated and called as such:
s = Solution()
print(s.snakesAndLadders(
[[-1,1,2,-1],[2,13,15,-1],[-1,10,-1,-1],[-1,6,2,8]]
))
print(s.snakesAndLadders(
[[-1,-1,-1,-1,-1,-1],[-1,-1,-1,-1,-1,-1],[-1,-1,-1,-1,-1,-1],[-1,35,-1,-1,13,-1],[-1,-1,-1,-1,-1,-1],[-1,15,-1,-1,-1,-1]]
))
