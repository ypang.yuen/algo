
import collections
from typing import List,Optional
import heapq
class Solution:
    def intersection(self, nums1: List[int], nums2: List[int]) -> List[int]:
        s = set(nums1)
        s2 = set(nums2)
        return s.intersection(s2)

# Your HitCounter object will be instantiated and called as such:
s = Solution()
s.intersection([1,2,2,1],[2,2])

