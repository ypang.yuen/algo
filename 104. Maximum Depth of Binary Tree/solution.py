import collections
from typing import Optional

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def maxDepth(self, root: Optional[TreeNode]) -> int:
      if not root: return 0
      return 1+max(self.maxDepth(root.left),self.maxDepth(root.right))


class Solution2:
    def maxDepth(self, root: Optional[TreeNode]) -> int:
        stack = [[root,1]]
        res = 0
        while stack:
            node, depth = stack.pop()
            if node:
                res = max(res, depth)
                stack.append([node.left, depth+1])
                stack.append([node.right, depth+1])
        return res
      

s = Solution2()
n=TreeNode(-3)
n.left = TreeNode(-9)
n.right = TreeNode(-3)

n.right.left = TreeNode(-4)

n.left.left = TreeNode(9)
n.left.left.left = TreeNode(6)
n.left.left.left.left = TreeNode(0)
n.left.left.left.right = TreeNode(6)
n.left.left.left.left.left = TreeNode(-1)
n.left.left.left.right.right = TreeNode(-4)

n.left.right = TreeNode(-7)
n.left.right.left = TreeNode(-6)
n.left.right.right = TreeNode(-6)
n.left.right.left.left = TreeNode(5)
n.left.right.right.left = TreeNode(9)
n.left.right.right.left.left = TreeNode(-2)
r=s.maxDepth(n)
print(r)

