import collections

class Trie:
    def __init__(self):
        self.d = collections.defaultdict(list)
        self.f = {}

    def insert(self, word: str) -> None:
        i = 0
        self.f[word] = word
        while i < len(word):
            c = word[:i+1]
            if len(self.d[c]) > 0:
                self.d[c].append(word)
            else:
                self.d[c] = [word]
            i+=1

    def search(self, word: str) -> bool:
        return word in self.f

    def startsWith(self, prefix: str) -> bool:
        return prefix in self.d

class TrieNode:
    def __init__(self):
        self.child = {}
        self.end = False

class Trie2:
    def __init__(self):
        self.root = TrieNode()

    def insert(self, word: str) -> None:
        cur = self.root
        for c in word:
            if c not in cur.child:
                cur.child[c] = TrieNode()
            cur = cur.child[c]
        cur.end = True

    def search(self, word: str) -> bool:
        cur = self.root
        for c in word:
            if c not in cur.child:
                return False
            cur = cur.child[c]
        return cur.end

    def startsWith(self, prefix: str) -> bool:
        cur = self.root
        for c in prefix:
            if c not in cur.child:
                return False
            cur = cur.child[c]
        return True
# Your Trie object will be instantiated and called as such:
# obj = Trie()
# obj.insert(word)
# param_2 = obj.search(word)
# param_3 = obj.startsWith(prefix)