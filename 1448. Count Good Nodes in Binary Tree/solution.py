import numbers
from typing import List, Optional
import collections
import sys
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

# dfs
class Solution:
    def rightSideView(self, root: Optional[TreeNode]) -> List[int]:
        res = 0
        if root is None: return res

        def dfs(root : Optional[TreeNode], maxVal:numbers):
            if root is None : 
                return 0
            # compare go first or after is not matter, as = or  > will add the total
            maxVal = max(root.val , maxVal)
            res = 1 if root.val >= maxVal else 0
            
            res += dfs(root.left, maxVal)
            res += dfs(root.right, maxVal)

            return res
            
        res += dfs(root, root.val )
        return res

# dfs with global var to shortern the required code
class Solution2:
    def goodNodes(self, root: TreeNode) -> int:
        self.count = 0
        def dfs(root, maxval):
            if not root: return

            # if cur val is larger than prev val, then we override the prev, and pass to next loop, else use the prev one
            if root.val >= maxval:
                self.count +=1
                maxval = root.val

            dfs(root.left, maxval)
            dfs(root.right, maxval)
        
        dfs(root, root.val)
        return self.count

s=Solution()
n=TreeNode(9, TreeNode(3, TreeNode(6, TreeNode(7,TreeNode(10)))), TreeNode(8, TreeNode(2,TreeNode(4), TreeNode(5))))
print(s.rightSideView(n))
