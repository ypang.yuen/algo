from typing import  List,Optional
import collections
class Solution:
    def getMaxLen(self, nums: List[int]) -> int:
        pos = neg = ans = 0
        for n in nums:
            if n > 0:
                pos += 1
                if neg:
                    neg += 1
            elif n < 0:
                tmp = pos
                if neg:
                    pos = neg +1
                else:
                    pos = 0
                neg = tmp +1
            else:
                neg = pos = 0
            ans = max(ans, pos)
        return ans