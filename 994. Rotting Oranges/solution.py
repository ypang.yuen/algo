
import collections
from typing import List

# Multiple BFS with topological sort
# Time: O(nxm), Space(nxm)
class Solution:
    def orangesRotting(self, grid: List[List[int]]) -> int:
        h, w = len(grid) , len(grid[0])
        c  = 0
        rotted = []
        hasOrange = False

        #find all rotted orange
        for j in range(h):
            for i in range(w):
                if grid[j][i] == 2:
                    rotted.append((j,i,0))
                    hasOrange = True
                elif grid[j][i] == 1:
                    hasOrange = True

        #there may be a case with no orange or no rotted orange
        if not hasOrange:
            return 0
        elif len(rotted) == 0:
            return -1

        #BFS at all rotted orange position at same time
        s = collections.deque(rotted)
        while s: 
            y,x,m = s.popleft()
            for Y,X in [(y+1,x),(y-1,x),(y,x+1),(y,x-1)]:
                if  0 <= Y < h and 0 <= X < w and grid[Y][X] == 1:
                    s.append((Y,X, m+1))
                    # Turn the orange to rotted to avoid next adj include the position
                    # We can also do the same task before get in to the loop by using a extra hash to record visited
                    grid[y][x] = 2 

        #Since m is promise to have, so counter will be the m
        c = m
        
        #Check if any flesh orange left
        for j in range(h):
            for i in range(w):
                if grid[j][i] == 1:
                    return -1
        return c 
s=Solution()
print(s.orangesRotting([[2,1,1],[1,1,1],[0,1,2]]))


