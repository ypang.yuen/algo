
from typing import List
class Solution:
    def maxSubArray(self, nums: List[int]) -> int:
        cur = gcur = nums[0]
        for n in range(1,len(nums)):
            cur = max(nums[n], nums[n]+cur)
            gcur = max(gcur, cur )
        return gcur


class Solution:
    def maxSubArray(self, nums: List[int]) -> int:
        maxVal = nums[0]
        for i in range(1, len(nums)):
            c = nums[i]
            p = nums[i-1]
            maxVal = max(c, maxVal)
            if c + p > c:
                maxVal = max(c+p, maxVal)
                nums[i] = c+p
        return maxVal

        
s=Solution()
print(s.maxSubArray([5,4,-1,7,8]))



s=Solution()        
r=s.maxSubArray([-2,1,-3,4,-1,2,1,-5,4])
print(r)


