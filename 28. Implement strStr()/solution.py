
import collections
from typing import List,Optional
from bisect import bisect_left
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution:
    def strStr(self, haystack: str, needle: str) -> int:
        L = 0
        for i in range(len(needle)-1, len(haystack)):
            if haystack[L:i+1] == needle:
                return L
            L+=1
        return -1

S=Solution()
haystack = "hello"
needle = "llo"
print(S.strStr(haystack,needle))


