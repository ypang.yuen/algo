
import collections
from typing import List,Optional
import heapq
class Solution:
    def minMeetingRooms(self, intervals: List[List[int]]) -> int:
        pq = []
        for i in intervals:
            heapq.heappush(pq,(i[0], 1))
            heapq.heappush(pq,(i[1], -1))
        maxRoom = 0
        directionSum = 0
        while pq:
            time, direction  = heapq.heappop(pq)
            directionSum += direction
            maxRoom = max(maxRoom, directionSum)
        return maxRoom
S = Solution()
print(S.minMeetingRooms([[0,30],[5,10],[15,20]]))
