
import collections
from typing import List,Optional

class Solution:
    def partition(self, s: str) -> List[List[str]]:
        output = []
        def isPalindrome(w):
            L = 0
            R = len(w)-1
            while L <= R:
                if w[L] != w[R]:
                    return False
                L+=1
                R-=1
            return True

        def dfs(start,  arr = []):

            if start == len(s):
                output.append(list(arr))
                return

            # we need the substring window horizontally increase, a -> ab -> abb -> abba
            # we need the substring window vertial increase, a -> b -> b -> a
            for i in range(start, len(s)):
                w = s[start:i+1]
                if isPalindrome(w):
                    arr.append(w)
                    dfs( i+1, arr)
                    arr.pop()
        
        dfs()
        return output


S = Solution()
S.partition("abba")