
import collections
from heapq import merge
from typing import List,Optional
import bisect
class Solution:
    def platesBetweenCandles(self, s: str, queries: List[List[int]]) -> List[int]:
        candles = [i for i,v in enumerate(list(s)) if v == '|']
        ans = []
        for s,e in queries:
            left = bisect.bisect_left(candles, s)
            right = bisect.bisect(candles, e)-1
            if left < right:
                totalDistance = candles[right] - candles[left]
                plate  = totalDistance - (right-left)
            else:
                plate = 0
            ans.append(plate)
        return ans