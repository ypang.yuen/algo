from typing import Optional
import collections
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution:

    def flip(self,root:Optional[TreeNode]):
        left = root.left 
        right = root.right
        root.left = right
        root.right = left
        if root.left:
            self.flip(root.left)
        if root.right:
            self.flip(root.right)

    def invertTree(self, root: Optional[TreeNode]) -> Optional[TreeNode]:
        self.flip(root)
        return root


s=Solution()
n=TreeNode(1, TreeNode(2,TreeNode(4), TreeNode(5)), TreeNode(3, TreeNode(6)))
r=s.invertTree(n)

q= collections.deque([r.left])
while q:
    v=q.popleft()
    print(v.val)
    if v.left:
        q.append(v.left)
    else:
        print("None")
    if v.right:
        q.append(v.right)
    else:
        print("None")

print("---------")

q= collections.deque([r.right])
while q:
    v=q.popleft()
    print(v.val)
    if v.left:
        q.append(v.left)
    else:
        print("None")
    if v.right:
        q.append(v.right)
    else:
        print("None")
