import collections
from typing import List

# DFS backtracking, O(n)
class Solution:
    def permuteUnique(self, nums: List[int]) -> List[List[int]]:
        
        #build hash map,{int:counter},  e.g [1,1,2] = {1:2, 2:1}
        counterHashMap = collections.Counter(nums)

        def backtrack(arr, hash):
            if len(arr) == n:
                output.append(list(arr))
                return 

            # instead of looping the nums, looping the hash to avoid duplicate 
            for num in hash: 
                # if hash is decreased to 0, means we have already use all value from the nums, hence we dont need to contine
                if hash[num] > 0:
                    hash[num]-=1
                    arr.append(num)
                    backtrack( arr, hash)
                    arr.pop()
                    hash[num]+=1

        n = len(nums)
        
        output = []
        backtrack([], counterHashMap)
        return output



S = Solution()
print(S.permuteUnique([1,2,2]))