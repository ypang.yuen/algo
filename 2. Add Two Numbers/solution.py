from textwrap import indent
from typing import Optional
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
class Solution:
    def addTwoNumbers(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        cur = l1
        cur2 = l2
        cur3 =  ListNode(999)
        carry = 0
        while cur and cur2:
            sum = cur.val+cur2.val + carry
            singleVal = sum % 10
            carry -= 1 if carry > 0 else 0
            carry += 1 if sum >= 10 else 0
            node  = ListNode(singleVal)
            node.next = cur3.next
            cur3.next  = node
            
            cur, cur2 = cur.next, cur2.next
            
        
        if cur:
            while cur:
                sum = cur.val + carry
                singleVal = sum % 10
                carry -= 1 if carry > 0 else 0
                carry += 1 if sum >= 10 else 0
                node  = ListNode(singleVal)
                node.next = cur3.next
                cur3.next  = node
                
                cur = cur.next

        if cur2:
            while cur2:
                sum = cur2.val + carry
                singleVal = sum % 10
                carry -= 1 if carry > 0 else 0
                carry += 1 if sum >= 10 else 0
                node  = ListNode(singleVal)
                node.next = cur3.next
                cur3.next  = node
                
                cur2 = cur2.next


        if carry == 1:
            node  = ListNode(1)
            node.next = cur3.next
            cur3.next  = node
 

        rev = None
        cur3 = cur3.next
        
        while cur3:
            rev, rev.next, cur3 = cur3, rev, cur3.next
        
        

        while rev:
            print(rev.val)
            rev = rev.next

class Solution2:
    def addTwoNumbers(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        dummy = ListNode(None)
        cur = dummy
        carry = 0
        while l1 or l2 or carry > 0:
            val1 = l1.val if l1 else 0
            val2 = l2.val if l2 else 0


            val = val1+ val2 + carry
            carry = val //10
            total = val %10
            cur.next = ListNode(total)
            cur = cur.next
            if l1:
                l1 = l1.next
            if l2:
                l2 = l2.next

        dummy = dummy.next
        while dummy:
            print(dummy.val)
            dummy = dummy.next
s=Solution2()
n=ListNode(5)
n.next=ListNode(5)
n2=ListNode(5)
n2.next=ListNode(5)
print(s.addTwoNumbers(n, n2))

