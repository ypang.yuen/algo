import collections
from typing import List,Optional
import heapq
# Back Tracking
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution:
    def boundaryOfBinaryTree(self, root: Optional[TreeNode]) -> List[int]:
        if not root.right and not root.left:
            return [root.val]

        left, right, center = [], [], []

        def dfsLeft(root):
            if not root or not root.right and not root.left:
                return
            left.append(root.val)
            if root.left:
                dfsLeft(root.left)
            else:
                dfsLeft(root.right)

        def dfsRight(root):
            if not root or not root.right and not root.left:
                return
            if root.right:
                dfsRight(root.right)
            else:
                dfsRight(root.left)
            right.append(root.val)

        def dfsCenter(root):
            if root.right:
                dfsCenter(root.right)
            if not root.right and not root.left:
                center.append(root.val)
            if root.left:
                dfsCenter(root.left)
            
        dfsLeft(root.left)
        dfsCenter(root)
        dfsRight(root.right)
        return [root.val] +  left + center[::-1] + right


