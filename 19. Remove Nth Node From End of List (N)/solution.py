from typing import Optional
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

## revise list and find the n and then revise back
class Solution:
    def removeNthFromEnd(self, head: Optional[ListNode], n: int) -> Optional[ListNode]:
        rev = None
        while head :
            rev , rev.next , head = head, rev, head.next
        i=0
        ans = None
        while rev:
            i+=1
            if i == n:
                rev =  rev.next
            else:
                ans, ans.next, rev = rev, ans, rev.next 
        return ans

## use the window of size n, shift the left , right pointer each iteration, then left pointer will equal to the nth item 
class Solution2:
    def removeNthFromEnd(self, head: Optional[ListNode], n: int) -> Optional[ListNode]:
        dummy = ListNode(0, head)
        left = dummy
        right = head
        while n > 0 and right:
            right = right.next
            n-=1
        
        while right:
            left = left.next
            right = right.next

        left.next = left.next.next

        return dummy.next


s=Solution2()
n=ListNode(1)
n.next=ListNode(2)

n.next.next=ListNode(3)
n.next.next.next=ListNode(4)
n.next.next.next.next=ListNode(5)
print(s.removeNthFromEnd(n, 2))

