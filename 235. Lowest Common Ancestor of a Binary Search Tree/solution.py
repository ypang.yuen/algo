# Definition for a binary tree node.
import collections


import collections
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

## O(n) solution, not the best
class Solution:
    def lowestCommonAncestor(self, root: TreeNode, p: TreeNode, q: TreeNode) -> TreeNode:
       stack = collections.deque([root])

       while stack : 
            c = stack.pop()
            if p.val <= c.val <= q.val or p.val >= c.val >= q.val:
                return c
            if c.left:
               stack.append(c.left)
            if c.right:
                stack.append(c.right)

## O(log n) solution
class Solution2:
    def lowestCommonAncestor(self, root: TreeNode, p: TreeNode, q: TreeNode) -> TreeNode:
       cur = root

       while cur : 
            if p.val > cur.val and q.val > cur.val:
                cur = cur.right
            elif p.val < cur.val and q.val < cur.val:
                cur = cur.left
            else:
                return cur

s=Solution()
r=s.lowestCommonAncestor( TreeNode(5,TreeNode(3), None), TreeNode(4), TreeNode(2) )
print(r.val)