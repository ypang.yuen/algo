
from ast import Lambda
import collections
from random import randrange
from typing import List,Optional
import heapq
# Back Tracking
class Solution:
    def wallsAndGates(self, grid: List[List[int]]) -> None:
        H, W = len(grid), len(grid[0])
        q = collections.deque([])

        for r in range(H):
            for c in range(W):
                if grid[r][c] == 0:
                    q.append((r,c,0))

        while q:
            for i in range(len(q)):
                r,c,step = q.popleft()
                for Y,X in [(r+1, c),(r-1, c),(r, c+1),(r, c-1)]:
                    if 0 <= Y < H and 0 <= X < W and grid[Y][X] > 0 and step+1 < grid[Y][X]:
                        grid[Y][X] = step+1
                        q.append((Y,X, step+1))

      
        
s=Solution()

a = [[2147483647,-1,0,2147483647],[2147483647,2147483647,2147483647,-1],[2147483647,-1,2147483647,-1],[0,-1,2147483647,2147483647]]
print(s.wallsAndGates(a))


             


