
from typing import List,Optional
class Solution:
    def nextGreaterElement(self, nums1: List[int], nums2: List[int]) -> List[int]:
        stack = []
        greater = {}
        for i in range(len(nums2)-1, -1, -1):
            n = nums2[i]
            while stack and stack[-1][1] <= n:
                stack.pop()
            
            if stack:
                greater[n] = stack[-1][1]
            else:
                greater[n] = -1
            
            stack.append((i, n))
        ans = []
        for n in nums1:
            ans.append(greater[n])
        return ans