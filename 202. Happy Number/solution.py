from re import L


class Solution:
    def isHappy(self, n: int) -> bool:
        if n == 1: return True
        stack = [str(n)]
        repeatCase = {}
        while stack:
            sn = stack.pop()
            total = 0
            for c in sn:
                total += int(c) ** 2
            if total == 1:
                return True
            if total in repeatCase:
                return False
            repeatCase[total] = 1
            stack.append(str(total))
        return False
s = Solution()
r=s.isHappy(2)
print(r)
