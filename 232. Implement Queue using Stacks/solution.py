class MyQueue:

    def __init__(self):
        self.stack = []
        self.stack2 = []

    def push(self, x: int) -> None:
        self.stack.append(x)

    def pop(self) -> int:
        f = self.stack[0]
        self.stack = self.stack[1:]
        return f

    def peek(self) -> int:
        return self.stack[0]

    def empty(self) -> bool:
        return len(self.stack) == 0




# Your MinStack object will be instantiated and called as such:
obj = MyQueue()

obj.push(0)
obj.push(-1)
param_1=obj.pop()
param_2=obj.peek()
param_3=obj.empty()
print(param_1, param_2, param_3)