from typing import List, Optional
import collections
from math import ceil
class Solution:
    def search(self, nums: List[int], target: int) -> int:
        L, R , l = 0 , len(nums)- 1, len(nums)
        
        while L <= R: 
            M = (L+R) // 2

            if nums[M] == target : return M


            if nums[M] < target:
                if nums[L] < nums[M] or nums[R] >= target:
                    L = M + 1
                else:
                    R = M - 1
            else:
                if nums[R] > nums[M] or nums[L] <= target:
                    R = M - 1
                else:
                    L = M + 1

        return -1
        

# 9 10 1 2 3 4 6


s=Solution()
print(s.search([4,5,6,9,10,0,1,2], 2))
print(s.search([4,5,6,9,10,0,1,2,3], 0))
print(s.search([4,5,6,9,10,0,1,2,3], 1))
print(s.search([4,5,6,9,10,0,1,2,3], 2))
print(s.search([4,5,6,9,10,0,1,2,3], 3))
print(s.search([4,5,6,9,10,0,1,2,3], 11))