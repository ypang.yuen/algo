
import collections
from typing import List,Optional
import heapq

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
        self.parent = None


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
class Solution:
    def rotateRight(self, head: Optional[ListNode], k: int) -> Optional[ListNode]:
        if head is None or head.next is None : return head
        cur = head
        nodeCount = 0
        while cur:
            nodeCount+=1
            cur = cur.next
            
        for i in range(k%nodeCount):
            last = slow = head
            while last.next:
                last = last.next

            while slow:
                if slow.next and not slow.next.next:
                    slow.next = None
                else:
                    slow = slow.next

            last.next = head
            head = last
       
        return head
    

s=Solution()
prev = ListNode(0)
cur = prev
for i in range(1,4):
    n = ListNode(i)
    cur.next = n
    cur = cur.next
c = prev.next
print(s.rotateRight(c, 200000))
 # while head:
            
        #     head = head.next
             


