from typing import List, Optional
import heapq
# Definition for a binary tree node.
class KthLargest:

    def __init__(self, k: int, nums: List[int]):
        self.k = k
        self.nums = nums

    def add(self, val: int) -> int:
        self.nums.append(val)
        return r


# Your KthLargest object will be instantiated and called as such:
obj = KthLargest(3, [4, 5, 8, 2])
param_1 = obj.add(3)
param_2 = obj.add(5)
param_3 = obj.add(10)
param_4 = obj.add(9)
param_5 = obj.add(4)

print(
param_1,
param_2,
param_3,
param_4,
param_5

)