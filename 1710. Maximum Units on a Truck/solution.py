
import collections
from ctypes.wintypes import tagMSG
from typing import List,Optional
from bisect import bisect_left, bisect_right
import heapq
import random
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Node:
    def __init__(self, val = 0, neighbors = None):
        self.val = val
        self.neighbors = neighbors if neighbors is not None else []

class Solution:
    def maximumUnits(self, boxTypes: List[List[int]], truckSize: int) -> int:
        count = 0
        pq = []
        for b in boxTypes:
            heapq.heappush(pq, (b[1]*-1, b[0]))

        while pq:
            unit, box = heapq.heappop(pq)
            
            if truckSize - box >= 0:
                truckSize -= box
                count += (box * unit * -1)
            else:
                count += ((box - (box-truckSize)) * unit * -1)
                return count

        return count

# Your Solution object will be instantiated and called as such:
s = Solution()
print(s.maximumUnits([[1,3],[5,5],[2,5],[4,2],[4,1],[3,1],[2,2],[1,3],[2,5],[3,2]], 35))

