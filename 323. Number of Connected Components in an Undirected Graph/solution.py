
from ast import Lambda
import collections
from itertools import cycle
from random import randrange
from typing import List,Optional
class Solution:
    def countComponents(self, n: int, edges: List[List[int]]) -> int:
        adj = {i : [] for i in range(n)}

        for e in edges:
            adj[e[0]].append(e[1])
            adj[e[1]].append(e[0])

        visit = set()
        def dfs(n):
            if n in visit:
                return False
            visit.add(n)
            for a in adj[n]:
                dfs(a)
            
        count = 0
        for i in range(n):
            if i not in visit:
                count +=1
                dfs(i)
        return count 
s = Solution()
a =  [[0,1],[1,2],[3,4]]
print(s.countComponents(5,a))