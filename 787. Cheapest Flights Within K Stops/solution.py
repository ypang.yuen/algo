
import collections
from typing import List,Optional
import heapq

class Solution:
    #bellman ford
    def findCheapestPrice(self, n: int, flights: List[List[int]], src: int, dst: int, k: int) -> int:
        if src == dst: return 0
        price = [float("inf")] * n
        price[src] = 0
      
        for i in range(k+1):
            tmpPrice = price.copy()
            for s,d,c in flights:
                if price[s] == float("inf"):
                    continue
                if price[s] + c < tmpPrice[d]:
                    tmpPrice[d] =  price[s] + c
            price = tmpPrice
            
        return price[dst] if price[dst] != float("inf") else -1

    #Dijkstra
    def findCheapestPrice(self, n: int, flights: List[List[int]], src: int, dst: int, k: int) -> int:
        weightMatrix = [[0]*n for _ in range(n)]
        dist = [float("inf")]*n
        stops = [float("inf")]*n
        for s,d,w in flights:
            weightMatrix[s][d] = w
        dist[src] = 0
        stops[src] = 0
        heap = [(0,0,src)]

        while heap:
            currW, stop, node = heapq.heappop(heap)

            if node == dst:
                return currW

            if stop == k+1:
                continue

            for _n in range(n):
                if weightMatrix[node][_n] > 0:
                    dV, wUV =  dist[_n], weightMatrix[node][_n]
                    if currW + wUV < dV:
                        dist[_n] = currW + wUV
                        stops[_n] = stop
                        heapq.heappush(heap, (currW + wUV, stop+1, _n))
                    elif stop < stops[_n]:
                        heapq.heappush(heap, (currW + wUV, stop+1, _n))
         
            
        return -1 if dist[dst] == float("inf") else dist[dst]
S = Solution()
n = 300
flights = [[0,200,100],[1,205,100],[0,300,500]]
src = 0
dst = 300
k = 1300000


# n = 10
# flights = [[3,4,4],[2,5,6],[4,7,10],[9,6,5],[7,4,4],[6,2,10],[6,8,6],[7,9,4],[1,5,4],[1,0,4],[9,7,3],[7,0,5],[6,5,8],[1,7,6],[4,0,9],[5,9,1],[8,7,3],[1,2,6],[4,1,5],[5,2,4],[1,9,1],[7,8,10],[0,4,2],[7,2,8]]
# src = 6
# dst = 0
# k = 8


print(S.findCheapestPrice(n, flights, src, dst, k))
