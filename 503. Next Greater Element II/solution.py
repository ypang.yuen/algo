
from typing import List,Optional
class Solution:
    def nextGreaterElements(self, nums: List[int]) -> List[int]:
        stack = []
        numsLen = len(nums)
        ans = [-1] * numsLen
        
        for j in range(2):
            for i in range(numsLen-1, -1, -1):
                n = nums[i]
                while stack and stack[-1][1] <= n:
                    stack.pop()
                if stack:
                    ans[i] = stack[-1][1]
                else:
                    ans[i] = -1
                stack.append((i,n))
                    
        return ans