from typing import Optional, List
import collections
class Solution:
    def firstUniqChar(self, s: str) -> int:
        d = {}
        for i in range(len(s)):
            c = s[i]
            count = 0
            if c in d:
                count,index = d.get(c)
            count+=1
            d[c] = (count, i)

        for k,c in d.items():
            count,index = c
            if count == 1:
                return index
        return -1

s=Solution()
print(s.firstUniqChar('teetcode'))
