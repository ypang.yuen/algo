from distutils.command.build import build
from platform import node
from typing import List, Optional
import collections

from regex import B
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

# pre order traversal, root left right, in order traversal, left root right 
# 1 . take left subtree, left of root in inorder = left subtree , root is 3, hence, left subtree is 1,9,2
# 2 . take right subtree, right of root in inorder = right subtree , root is 3, hence, right subtree is 15,20,7
# 3 . take left subtree from 1,9,2, root is prev index + 1 in preorder , hence, left subtree is 1
# 4 . take right subtree from 1,9,2, root is prev index + 1 in preorder , hence, left subtree is 2

class Solution:
    def buildTree(self, preorder: List[int], inorder: List[int]) -> Optional[TreeNode]:
        if not preorder or not inorder: return None
        
        root = preorder[0]
        node = TreeNode(root)
        mid = inorder.index(root)
        
        # start from 1 on preorder, becoz it we have already used the first val to build the root tree
        node.left = self.buildTree(preorder[1:mid+1], inorder[:mid])

        node.right = self.buildTree(preorder[mid+1:], inorder[mid+1:])

        return  node


s=Solution()
r=s.buildTree([3,9,1,2,20,15,7], [1,9,2,3,15,20,7])
print(r)
