from typing import List, Optional
import collections
from math import ceil
class Solution:
    def minEatingSpeed(self, piles: List[int], h: int) -> int:
        maxP = max(piles)
        minK = maxP
        L, R = 1 ,maxP
        
        while L <= R:
            K = (L+R) // 2
            hrToEat = 0
         #   print(L, R, K)
            for p in piles:
                hrToEat += ceil(p/K)
            if hrToEat > h:
                L = K+1
            if hrToEat <= h:
                R = K-1
                minK = min(minK, K)
                
        return minK
        

s=Solution()
print(s.minEatingSpeed([3,6,7,11], 8))
print(s.minEatingSpeed([3,6,7,11], 7))
print(s.minEatingSpeed([30,11,23,4,20], 5))
print(s.minEatingSpeed([30,11,23,4,20], 6))
print(s.minEatingSpeed([312884470], 968709470))
