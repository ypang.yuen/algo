import collections
from typing import List

# having a binary decision tree to include or not include
#                      []
#           [1]                  []         //include 1 or not include
#     [1,2]      [1]          [2]     []    //include 2 or not include
# [1,2,3] [1,2]  [3,1][1] [2,3] [2] [3] []  //include 3 or not include
class Solution:
    def subsets(self, nums: List[int]) -> List[List[int]]:

        def backtrack(i = 0,  arr = []):
            
            if i >= n:
                output.append(list(arr))
                return
                
            arr.append(nums[i])
            backtrack(i + 1, arr)
            arr.pop()
            #including the tree that without the nums[i]
            backtrack(i + 1, arr)

        n = len(nums)
        output = []
        backtrack()
        return output


## standard backtracking without convert it into binary tree
class Solution:
    def subsets(self, nums: List[int]) -> List[List[int]]:
        output = []

        def dfs(arr = [] , n = 0):
            # doesnt need to have condition, always plug the arr to output
            output.append(list(arr))
            
            # use i as the start point to keep increment the curson
            for i in range(n, len(nums)):
                arr.append(nums[i])
                # increment 1 to i to avoid repeat at same index
                dfs(arr, i+1)
                arr.pop()
        dfs()

        return output


S = Solution()
print(S.subsets([1,2,3]))