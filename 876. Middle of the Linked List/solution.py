from typing import Optional
# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def middleNode(self, head: Optional[ListNode]) -> Optional[ListNode]:
        temp = head
        len = 0
        while temp:
            len+=1
            temp = temp.next 
        mid = len // 2 + 1
        c = 0
        while head:
            c+=1
            if c == mid:
                return head
            head = head.next
        return head
        
class Solution2:
    def middleNode(self, head: Optional[ListNode]) -> Optional[ListNode]:
        slow = fast = head
        while fast and fast.next:
            fast = fast.next.next
            slow = slow.next
        return slow
s = Solution2()
a1=a=ListNode(0)
v=1
while v <= 6:
  a.next = ListNode(v)
  a = a.next
  v+=1

r = s.middleNode(a1.next)
print(r.val)