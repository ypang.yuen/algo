
import collections
from typing import List
class Node:
    def __init__(self, val = 0, neighbors = None):
        self.val = val
        self.neighbors = neighbors if neighbors is not None else []

# DFS to recursive loop the node and copy in hash
class Solution:
    def cloneGraph(self, node: Node) -> Node:
        if not node : return None
        clone = {}
        def dfs(node):
            if node in clone:
                return clone[node] # if node has cached we return the cached one , to avoid inf loop
            newNode = Node(node.val)
            clone[node] = newNode # cache the node
            for n in node.neighbors :
                if n not in clone:
                    clone[node].neighbors.append(dfs(n))
            return clone[node]
        return dfs(node)
a =  Node(1)
b =  Node(2)
c =  Node(3)
d =  Node(4)

a.neighbors = [b,d]
b.neighbors = [a,c]
c.neighbors = [b,d]
d.neighbors = [a,c] 
s=Solution()
r = s.cloneGraph(a)


s = collections.deque([r])
while s:
    n = s.popleft()
    print(n.val)
    for a in n.neighbors:
        s.append(a)


