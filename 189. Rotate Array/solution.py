
import collections
from typing import List,Optional
class Solution:
    def rotate(self, nums: List[int], k: int) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        def swap(i,j):
            temp = nums[i]
            nums[i] = nums[j]
            nums[j] = temp
        def reverse(L,R):
            while L < R:
                swap(L,R)
                L+=1
                R-=1
        
        #k is the steps to rotates, hence it may larger than the len of num
        k %= len(nums)
        
        # reverse the whole arr, 1234567 => 7654321
        reverse(0, len(nums)-1)

        # reverse the arr till k, if k = 2 7654321 => 6754321
        reverse(0, k-1)

        # reverse the from k till en, if k = 2 6754321 => 6712345
        reverse(k, len(nums)-1)

        #ans 1234567 , move k = 2 steps = 6712345
S=Solution()
s.rotate([1,2,3,4],12)
