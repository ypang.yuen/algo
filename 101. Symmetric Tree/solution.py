import collections
from typing import Optional, List
# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
        
class Solution:
    def isSymmetric(self, root: Optional[TreeNode]) -> bool:

        def invent(root):
            if not root : return
            root.left, root.right = root.right, root.left
            if root.left:
                invent(root.left)
            if root.right:
                invent(root.right)
                
        invent(root.left)
        
        def isSameTree(p, q):
           if p is None and q is None: return True
           if p is None or q is None or p.val != q.val: return False
           return isSameTree(p.left, q.left) and isSameTree(p.right, q.right)

        return isSameTree(root.left, root.right)
            

s=Solution()
n=TreeNode(1,  TreeNode(2,TreeNode(4), TreeNode(3)), TreeNode(2,TreeNode(4), TreeNode(3)))
r=s.isSymmetric(n)

