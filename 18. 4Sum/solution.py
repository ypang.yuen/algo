
import collections
from heapq import merge

from typing import List,Optional
#
class Solution:
    def fourSum(self, nums: List[int], target: int) -> List[List[int]]:
        l = len(nums)
        nums.sort()
        ans = set()
        for i in range(l):
            if i > 0 and nums[i] == nums[i-1]:
                continue
            n1 = nums[i]
            for j in range(i+1, l):
                if i > i+1 and nums[j] == nums[j-1]:
                    continue
                n2 = nums[j]
                L = j+1
                R = l-1
                while L < R:
                    total = n1 + n2 + nums[L] + nums[R]
                    if total == target:
                        ans.add(tuple((n1,n2,nums[L],nums[R])))
                    if total > target:
                        R-=1
                    else:
                        L+=1
        
        return ans
            
            

S = Solution()
print(S.fourSum([2,2,2,2,2], 8))