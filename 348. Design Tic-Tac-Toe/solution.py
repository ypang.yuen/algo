
from typing import List,Optional

#O(n)
class TicTacToe:

    def __init__(self, n: int):
        self.board = [['#' for i in range(n)] for i in range(n)]
        self.winner = 0

    def move(self, row: int, col: int, player: int) -> int:
        if self.winner > 0 :
            return self.winner

        self.board[row][col] = player
        n = len(self.board) 
        a = b =c =d = 0
        for i in range(n):
            if self.board[row][i] == player:
                a+=1
            if self.board[i][col] == player:
                b+=1
            if self.board[i][i] == player:
                c+=1
            if self.board[i][n-i-1] == player:
                d+=1
        if a == n or b == n or c == n or d== n:
            return player
        return 0
#O(1)    
class TicTacToe:

    def __init__(self, n: int):
        self.R = [0] * n 
        self.C = [0] * n 
        self.D = 0
        self.AD = 0
        self.N = n

    def move(self, row: int, col: int, player: int) -> int:
        if player == 1:
            self.R[row] +=1
            self.C[col] +=1
            if row-col == 0:
               self.D+=1
            if row+col == self.N - 1:
               self.AD+=1
        else:
            self.R[row] -=1
            self.C[col] -=1
            if row-col == 0:
               self.D-=1
            if row+col == self.N - 1:
               self.AD-=1

        if abs(self.R[row]) == self.N or abs(self.C[col]) == self.N or abs(self.D) == self.N or abs(self.AD) == self.N:
            return player
        
        return 0


       

obj = TicTacToe(2)
param_1 = obj.move(0, 1, 1)
param_1 = obj.move(1, 1, 2)
param_1 = obj.move(1, 0, 1)
print(param_1)