
import collections
from heapq import heapify, merge,_heapify_max,_heappop_max
from typing import List,Optional
from bisect import bisect_left
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
class Solution:

    def findCloesetIndex(self, arr, x):
        if x > arr[-1]:
            return len(arr) 
        if x < arr[0]:
            return 0
        L , R = 0,len(arr) - 1
        minDistance = len(arr)-1
        index = 0
        while L <= R:
            mid = (L+R)//2
            val = arr[mid]
            if val == x:
                index = mid
                break
            if abs(x-val) < minDistance:
                index = mid
                minDistance = abs(x-val)
            if val > x:
                R = mid - 1
            else:
                L = mid + 1 
        return index

    def findClosestElements(self, arr: List[int], k: int, x: int) -> List[int]:
        
        # find item index, if item is not exist in the list, it will return the closest index, 
        # e.g [1,2,3,4,6], find 5 , will return index 3, 4 and 6 is cloeset to 5 , but choose smaller number
        mid = self.findCloesetIndex(arr, x)

        left = mid-1
        
        right = mid
        
        #using slide window, loop when slide window < k
        while (right - left - 1) < k:
            # if left is out of 0 ,then we continue to right
            if left == -1:
                right +=1
                continue
            
            # we need to move left index when:
            # 1. right reach to the end, but the slide window still smaller than k
            # 2. left side cloeset to x , if left and right equal, we choose left
            if right == len(arr) or abs(x - arr[left]) <= abs(x - arr[right]):
                left-=1
            else:
                right+=1     
        # becoz we start the slide window from mid -1 as left, therefore, when we out put the value from the slide window, we need to add 1
        return arr[left + 1:right]
S=Solution()
arr = [1,2,3,4,9,10,11]
k = 2
x = 5
print(S.findClosestElements(arr,k,x))