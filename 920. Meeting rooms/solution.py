import heapq
from typing import List
class Internval:
    def __init__(self, start, end) -> None:
        self.start = start
        self.end = end

class Solution:
    def canAttendMeeting(self, internvals: List[Internval]) -> bool:
        internvals.sort(key= lambda x: x.start)
        end=internvals[0].end
        for i in range(1, len(internvals)):
            cur = internvals[i]
            if cur.start <= end:
                return False
            end = cur.end
        return True

class Solution2:
    def canAttendMeeting(self, internvals: List[Internval]) -> bool:
        stack = []
        for x in internvals:
            heapq.heappush(stack, [x.start, 1])
            heapq.heappush(stack, [x.end, -1])
        total = 0
        while stack: 
            start, c = heapq.heappop(stack)
            total += c
            if total > 1:
                return False

        return True
s=Solution2()
r=s.canAttendMeeting([Internval(5,16),Internval(15,20),Internval(1,2)])
print(r)