import collections
from typing import List
##bucket sort, O(N) runtime, O(N) space
class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        bucket = [0] * (len(nums)+1)
        d = collections.defaultdict(int)
        for n in nums:
            d[n]+=1
        for k,c in d.items():
            bucket[c]=k
        return bucket[1]

class Solution2:
    def singleNumber(self, nums: List[int]) -> int:
        for i in range(1, len(nums)):
           nums[i] ^= nums[i-1]
        return nums[len(nums)-1]
s=Solution2()
print(s.singleNumber([1,2,1,2,3,5,3]))