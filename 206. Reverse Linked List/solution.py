from typing import Optional
# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def reverseList(self, head: Optional[ListNode]) -> Optional[ListNode]:
      res = None
      while head:
        t = head.next
        head.next = res
        res = head
        head = t

        res, res.next, head  = head, res, head.next
      return res
      
# head = 12345

# res = 12345
# res.next = None
# head = 2345

# head = 2345
# res = 2345
# res.next = 1
# head = 345

# head = 345
# res = 345
# res.next = 2,1
# head = 45

# head = 45
# res = 45
# res.next = 3,2,1
# head = 5

# head = 5
# res = 5
# res.next = 4,3,2,1
# head = None
# res = 5,4,3,2,1
s = Solution()
a1=a=ListNode(0)
v=1
while v <= 5:
  a.next = ListNode(v)
  a = a.next
  v+=1

r = s.reverseList(a1.next)

while r:
  print(r.val)
  r = r.next
