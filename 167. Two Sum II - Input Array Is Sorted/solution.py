from typing import List

class Solution:
    def twoSum(self, numbers: List[int], target: int) -> List[int]:
        if len(numbers) == 2: return [1,2] 
        left, right = 0, len(numbers) -1
        while left <= right:
            l = numbers[left]
            r = numbers[right]
            s = l+r
            if s == target:
                return [left + 1, right +1]
            if s > target:
                right-=1
            else:
                left+=1

s=Solution()
print(s.twoSum([-1,0], -1))