from cgitb import strong
import collections
from typing import List, Optional
from heapq import heapify, _heapify_max, _heappop_max, _heapreplace_max, heappush, heappop
class Solution:
    def lastStoneWeight(self, stones: List[int]) -> int:
        stones = [-x for x in stones]
        heapify(stones)
        while stones:
            n1 = heappop(stones)
            if len(stones) == 0:
                return abs(n1)
            n2 = heappop(stones)
            r =  n1-n2
            if r != 0:
                heappush(stones, r)
        return 0
            

s=Solution()
r=s.lastStoneWeight([7,6,7,-6,9])
print(r)



# 9,7 [7,6,6,2]
# 7,6 [6,2,1]
# 6,2 [4,1]
# 4,1 [3]
