import collections
from typing import List, Optional
class Solution:
    def fizzBuzz(self, n: int) -> List[str]:
        res = []
        for i in range(n):
            t = i+1
            if t%3 == 0 and t%5 == 0:
                res.append("FizzBuzz")
            elif t%3 == 0:
                res.append("Fizz")
            elif t%5 == 0:
                res.append("Buzz")
            else:
                res.append(str(t))
        
        return res