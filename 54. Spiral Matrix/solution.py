import collections
from typing import List

class Solution:
    def spiralOrder(self, matrix: List[List[int]]) -> List[int]:
        w, h = len(matrix[0]), len(matrix)
        res = []
        yL,yR,xL,xR = 0, h-1, 0, w-1 
        y,x = 0,0
        while len(res) < w*h:
            cor = matrix[y][x]
            res.append(cor)
            if  y == yL and x < xR:
                x+=1
            elif  x == xR and y < yR:
                y+=1
            elif  y == yR and x > xL:
                if  x-1 == xL:
                    yL+=1
                    yR-=1
                x-=1
            elif  x == xL and y > yL:
                if y-1 == yL:
                    xL+=1
                    xR-=1
                y-=1
        return res
                        
S = Solution()
matrix = [[1,2,3,4],[5,6,7,8],[9,10,11,12],[13,14,15,16]]
print(S.spiralOrder(matrix))