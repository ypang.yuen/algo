
import collections
from typing import List,Optional
import heapq

class Solution:
    def reorganizeString(self, s: str) -> str:
        hash = collections.Counter(s)
        pq = []
        for c in hash:
            heapq.heappush(pq, (hash[c]*-1, c))
        newS = ''
        prevC = ''
        pushBack = None
        while pq:
            freq , c = heapq.heappop(pq)
            if c == prevC:
                pushBack = (freq , c)
                continue
            newS += c
            prevC = c
            print(c)
            if freq + 1 < 0:
                heapq.heappush(pq, (freq+1, c))
            if pushBack:
                heapq.heappush(pq, pushBack)
                pushBack = None
        
        return newS




s=Solution()
a =  "vvvlo"
a = "aabbccc"
a = "aabbcc"
a = "aaabbccc"
a = "aabbcccdddddddd"
a = "bfrbs"
print(s.reorganizeString(a))


             


