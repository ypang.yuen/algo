
from typing import List
class Solution:
    def merge(self, intervals: List[List[int]]) -> List[List[int]]:
        input=sorted(intervals, key=lambda x:x[0])
        res=[input[0]]
        prev = input[0]
        for i in range(1,len(input)):
            next = input[i]
            curStart, curEnd = next
            prevEnd = prev[0]
            if prevEnd >= curStart:
                prev[1] =  max(curEnd, prevEnd)
            else:
                res.append(next)  
                prev = next
        return res
     


class Solution2:
    def merge(self, intervals: List[List[int]]) -> List[List[int]]:
        d = []
        for i in intervals:
            start, end = i
            d.append([start, 1])
            d.append([end, -1])

        d.sort(key = lambda x : (-x[0], x[1]))
        
        total = 0
        res = []
        ans = []
        while d:
            time, dir = d.pop() 
            total += dir
            if len(res) == 0:
                res.append(time)
            if total == 0:
                res.append(time)
                ans.append(list(res))
                res = []
        return ans

        
s=Solution()
print(s.merge([[1,4],[4,5],[8,22],[15,18]]))


