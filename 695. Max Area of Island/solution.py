
from ast import Lambda
import collections
from typing import List,Optional
import heapq
# Back Tracking
class Solution:
    def maxAreaOfIsland(self, grid: List[List[int]]) -> int:
        H, W = len(grid), len(grid[0])
        
        def dfs(r,c, area = 0):
            grid[r][c] = 2
            for Y,X in [(r+1, c),(r-1, c),(r, c+1),(r, c-1)]:
                if 0 <= Y < H and 0 <= X < W and grid[Y][X] == 1:
                    area = max(area, dfs(Y,X, area))
            return 1+area
        res = 0
        for r in range(H):
            for c in range(W):
                if grid[r][c] == 1:
                    area = dfs(r,c)
                    res = max(area, res)

        return res
s=Solution()

a = [[1,1,0,0,0],[1,1,0,0,0],[0,0,0,1,1],[0,0,0,1,1]]
print(s.maxAreaOfIsland(a))


             


