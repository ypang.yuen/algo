from typing import List, Optional
import collections
import heapq
class TimeMap:

    def __init__(self):
        self.d = collections.defaultdict(list)

    def set(self, key: str, value: str, timestamp: int) -> None:
        if len(self.d[key]) > 0:
            self.d[key].append((timestamp,value))
        else:
            self.d[key] = [(timestamp,value)]

    def get(self, key: str, timestamp: int) -> str:
        if key not in self.d:
            return ''
        
        vals = self.d[key]
        L , R = 0 , len(vals) - 1
        # init a2 val to record the most closest timestamp to tagret, and the assoicated val
        close, closeVal = 0, ''
        while L <= R:
            M = (L+R) // 2
            mid, minVal = vals[M]

            # if the found is <= timestamp, update the closest timestamp to found
            # e.g 
            # given timestamp is 5
            # if found is 3 , 3<=5, update the closest timestamp to 3
            # then if next found is 4. 4<=5 , update the closest timestamp to 4
            # then if next found is 6. 6<=5 , move cursor
            # and the end, we has already record the most closet val
            
            if mid <= timestamp:
                if mid > close:
                    close = mid
                    closeVal = minVal

            if mid < timestamp:
                L = M+1
            else:
                R = M-1
        
        return closeVal


obj = TimeMap()
obj.set("foo", "bar", 1)
obj.set("foo", "bar2", 2)
obj.set("foo", "bar3", 3)
# print(obj.get("foo", 1))
# print(obj.get("foo", 3))
obj.set("foo", "bar5", 5)
#print(obj.get("foo", 4))
print(obj.get("foo", 4))