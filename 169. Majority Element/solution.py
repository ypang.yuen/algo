import collections
from typing import List
class Solution:
    def majorityElement(self, nums: List[int]) -> int:
      d=collections.defaultdict(int)
      baseLine = len(nums) // 2 
      for x in nums:
        d[x] += 1
        if d[x] >= baseLine:
          return x
      return None
         

# s = Solution()
# r=s.majorityElement([3,3,3,3,2,2,2,1,1,3])
# print(r)

class Solution2:
    def majorityElement(self, nums: List[int]) -> int:
      c=nums[0]
      v=0
      for n in nums:
        if v == 0:
          v = 1
          c = n
        else:
          if n == c:
            v+=1
          else:
            v-=1
      return c

s = Solution2()
r=s.majorityElement([3,3,3,3,3,3,3,3,1,1,1,1,1,1,13,3,3,3,2,9,8,7,6,3,3])
print(r)
