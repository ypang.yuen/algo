
import collections
from heapq import merge

from typing import List,Optional
class Solution:
    def canCompleteCircuit(self, gas: List[int], cost: List[int]) -> int:
        if sum(gas) < sum(cost):
            return -1
        for i in range(len(gas)):
            gas[i] = gas[i]-cost[i]
        
        tank = 0
        start = 0
        for i in range(len(gas)):
            if tank + gas[i] >= 0:
                tank += gas[i]
            else : 
                tank = 0
                start = i+1
        return start
S = Solution()
print(S.canCompleteCircuit([5,1,2,3,4], [4,4,1,5,1]))

