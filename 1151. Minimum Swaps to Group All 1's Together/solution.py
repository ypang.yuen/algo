
from typing import  List,Optional
import collections
#O(n)
class Solution:
    def minSwaps(self, data: List[int]) -> int:
        L, R = 0 , 0
        winSize = sum(data)
        if winSize == 1 or winSize == len(data):
            return 0
        one = maxOne = 0
        while R < len(data):
            one += data[R]
            R+=1
            if R+L > winSize:
                one -= data[L]
                L+=1
            maxOne = max(maxOne, one)
        return winSize - maxOne
       

s = Solution()
print(s.minSwaps([1,0,1,0,1,0]))
