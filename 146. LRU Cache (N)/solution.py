import collections
from typing import Optional
class LRUCache:

    def __init__(self, capacity: int):
        self.cap = capacity
        self.recent = collections.deque([])
        self.cache = {}

    def get(self, key: int) -> int:
        if key in self.cache:
            return self.cache[key]
        return -1

    def put(self, key: int, value: int) -> None:
            if len(self.recent) >= self.cap:
                v = self.recent.pop()
                self.cache.pop(v)
                self.recent.appendleft(key)
            else:
                self.recent.append(key)
                
            self.cache[key] = value
        


obj = LRUCache(2)

obj.put(1,1)
obj.put(2,2)
obj.put(2,1)
obj.put(3,3)
obj.put(4,4)
print(obj.get(1))
print(obj.get(3))
print(obj.get(4))