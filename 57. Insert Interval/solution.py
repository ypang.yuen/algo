from typing import List
import heapq
class Solution:
    def insert(self, intervals: List[List[int]], newInterval: List[int]) -> List[List[int]]:
        heap = []
        heap.append((newInterval[0],1))
        heap.append((newInterval[1],-1))
        for n in intervals:
            heap.append((n[0],1))
            heap.append((n[1],-1))
        heap.sort(key = lambda y: (-y[0], y[1]))
        res = []
        stack = []
        while heap:
            time, direction = heap.pop()
            if direction == -1:
                s = stack.pop()
                if len(stack) < 1:
                    res.append([s, time])
            else:
                stack.append(time)
        return res
s=Solution()
print(s.insert([[1,2],[3,5],[6,7],[8,10],[12,16]], [18,19]))
print(s.insert([[1,3],[6,9]], [2,5]))


# 1 = +1
# 2 = -1

# 3 = +1
# 4 = +1
# 5 = -1
# 6 = +1
# 7 = -1
# 8 = +1
# 8 = - 1
# 10 = -1


# 12 = +1
# 16 = -1

1