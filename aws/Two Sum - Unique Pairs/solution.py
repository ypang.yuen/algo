import collections
from typing import List
##hash
class Solution2:
    def two_sum_unique_pairs(self, nums: List[int], target: int) -> int:
        d = collections.defaultdict(int)
        for i in range(0, len(nums)):
            remain = target-nums[i]
            if remain not in d:
                d[nums[i]] = 0
        return len(d.values())
##pointer
class Solution:
    def two_sum_unique_pairs(self, nums: List[int], target: int) -> int:
        nums.sort()
        l, r = 0, len(nums) - 1
        d={}
        while l < r:
            t  = nums[l] + nums[r]
            if t == target: 
                if nums[l] not in d and nums[r] not in d:
                    d[nums[l]] = nums[r]
                r-=1
                continue
            if t > target:
                r-=1
            elif t < target:
                l+=1
        return len(d.values())


s=Solution()
print(s.two_sum_unique_pairs([3, 1, 5, 3, 3], 6))
print(s.two_sum_unique_pairs([1,5,1,5], 6))        

s=Solution2()
print(s.two_sum_unique_pairs([3, 1, 5, 3, 3], 6))
print(s.two_sum_unique_pairs([1,5,1,5], 6))



