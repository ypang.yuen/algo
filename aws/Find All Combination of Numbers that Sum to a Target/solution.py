from typing import List
import collections
def number_of_options(target) -> int:
    result = []
    stack = collections.deque(list(range(1, target+1)))
    o = 1


    for c in range(1, target+1):
        remain = target - c
        while stack:
            c = stack.popleft()
            remain = target - c
            if remain != 0 :
                if (remain, c) not in result and  (c, remain) not in result:
                    result.append((remain, c))
            if target-1 - remain != 0:
                stack.append(remain)
    
    # while stack:
    #     c = stack.popleft()

    #     remain = target - c
    #     if remain != 0 :
    #         if (remain, c) not in result and  (c, remain) not in result:
    #            result.append((remain, c))
               
    #     if target-1 - remain != 0:
    #         stack.append(remain)
        
    #     print(c, target)
    #     if len(stack) == target - 2 :
    #         target-=1
           

        
        



print(number_of_options(5))
