def is_robot_bounded(movements: str) -> bool:
    x, y , dx , dy = 0, 0, 0, 1

    for m in movements:
        if m == 'G':
            x+=dx
            y+=dy
        elif m == 'L':
            dx = -dy
            dy = dx
        else:
            dx = dy
            dy = -dx

    return (x, y) == (0, 0) or (dx, dy) != (0, 1)


print(is_robot_bounded("GGLLGG"))

print(is_robot_bounded("GL"))