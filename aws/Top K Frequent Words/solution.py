import collections
from typing import List
class Solution:
    def topKFrequent(self, words: List[str], k: int) -> List[str]:
        if len(words) == 1: return words
        bucket = [[]] * (len(words) + 1)
        d = collections.defaultdict(int)        
        for w in words:
            d[w] += 1
            
        for w, count in d.items():
            if len(bucket[count]) <= 0 :
                bucket[count] = [w]
            else:
                bucket[count] += [w]

        res = []
        for i in range(len(bucket)-1, -1, -1):
            items = bucket[i]
            items.sort()
            for w in items:                
                if len(res) < k:
                    res.append(w)
                else:
                    break
        return res


s=Solution()
r=s.topKFrequent(["i","love","l","i","love","lc"], 3)
print(r)
