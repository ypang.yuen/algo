from typing import List
def slowestKey( releaseTimes: List[int], keysPressed: str) -> str:
        slowest = releaseTimes[0]
        slowestChar = keysPressed[0]

        for i in range(1, len(releaseTimes)):
            current = releaseTimes[i]
            prev = releaseTimes[i-1]
            duration = current - prev
            if duration > slowest:
                slowestChar = keysPressed[i]
                slowest = duration

            if duration == slowest and ord(keysPressed[i]) > ord(slowestChar):
                slowestChar = keysPressed[i]
        
     
        return slowestChar

        
        



print(slowestKey([9,29,49,50], "cbcd"))
