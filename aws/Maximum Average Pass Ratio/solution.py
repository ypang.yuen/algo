from typing import List
def maxAverageRatio( classes: List[List[int]], extraStudents: int) -> float:
    maxProfit = (((extraStudents+classes[0][0]) / (extraStudents+classes[0][1])) -  (classes[0][0]/classes[0][1])  ,0)
    
    for i in range(1, len(classes)):
        passStud, totalStud = classes[i]
        profit = ((extraStudents+passStud) / (extraStudents+totalStud)) -  (passStud/totalStud)
        _maxProfit, index = maxProfit
        if profit > _maxProfit:
            maxProfit = (profit, i)
           

    
    _max, index = maxProfit
    index = 0

    while index < len(classes):
        avg = 0
        for i in range(0, len(classes)):
                passStud, totalStud = classes[i]
                if i == index: 
                    passStud += extraStudents
                    totalStud += extraStudents
                avg += (passStud/totalStud)
        print(index, avg/len(classes))
        index+=1
    
    return round(avg/len(classes), 5)

        
        



print(maxAverageRatio([[2,4],[3,9],[4,5],[2,10]], 4))

