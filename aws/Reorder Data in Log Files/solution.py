import collections
import heapq
from typing import List
class Solution:
    def reorderLogFiles(self, logs: List[str]) -> List[str]:
        digitList = []
        letterList = []
        res = []
        for w in logs:
            if 48 <= ord(w[-1])  <= 57 : 
                digitList.append(w)
            else: 
                t = w.split(' ', 1)
                content,id = t[1], t[0]
                heapq.heappush(letterList, (content, id))
        
        while letterList:
            k, v = heapq.heappop(letterList)
            res.append(k+' '+v)

        return res + digitList


s=Solution()
r=s.reorderLogFiles(["dig1 8 1 5 1","let1 art zero can","dig2 3 6","let2 own kit dig","let3 art zero"])
print(r)
