from cgitb import strong
import collections
from typing import List, Optional
from heapq import heapify, _heapify_max, _heappop_max, _heapreplace_max, heappush, heappop
import time

## use min heap pop out the smallest, leave the range of K
class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        heapify(nums)
        for i in range(len(nums)-k):
            heappop(nums)
        return heappop(nums)

## use max heap, only pop out the largest until reach K time
class Solution2:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        _heapify_max(nums)
        while k > 0:
            val = _heappop_max(nums)
            k-=1
            if k == 0 :
                return val

n = list(range(1, 50000))
start_time = time.time()
s = Solution2()
print(s.findKthLargest(n , 4))
print("--- %s seconds ---" % (time.time() - start_time))

start_time = time.time()
s = Solution2()
print(s.findKthLargest(n, 4))
print("--- %s seconds ---" % ( (time.time() - start_time)))
