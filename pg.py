import collections
from typing import List

# having a binary decision tree to include or not include
#                      []
#           [1]                  []         //include 1 or not include
#     [1,2]      [1]          [2]     []    //include 2 or not include
# [1,2,3] [1,2]  [3,1][1] [2,3] [2] [3] []  //include 3 or not include
class Solution:
    def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:
        output = []
        def backtrack(arr = [], n = 0, start = 0):
            if n == target:
                output.append(list(arr))
                return
            # passing start to avoid using same combination
            for i in range(start, len(candidates)):
                c = candidates[i]
                if c > target or c+n > target : 
                    continue
                arr.append(c)
                # passing i instead of i+1 , becoz we allow duplication on the usage of number, but not the combination
                backtrack(arr, n+c, i)
                arr.pop()
        backtrack()
        return output

S = Solution()

print(S.combinationSum([2,3,5],8))

