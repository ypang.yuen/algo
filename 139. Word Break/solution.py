
from ast import Lambda
import collections
from typing import List,Optional
import heapq
class Solution:
    def wordBreak(self, s: str, wordDict: List[str]) -> bool:
        q = collections.deque([0])
        dict = set(wordDict)
        seen = {}
        seen[0] = 1
        while q:
            start = q.popleft()
            if start == len(s):
                return True
            for i in range(start, len(s) +1):
                if s[start:i] in dict and i not in seen:
                    seen[i] = 1
                    q.append(i)
        return False
s=Solution()
st = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab"
d = ["a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"]

print(s.wordBreak(st, d))

             


