# algo

### prerequisite
1. buy leetcode premium


### Algo Study Prioity 
1. DFS + BackTracking (key is to understand how to use backtracking to during the DFS)
2. BFS + Bi-Direction BFS (key is to understand when to use BFS and how to do bi direction BFS)
3. Binary Search (key is to understand when will be the situration of using BS, beside of finding larger or smaller from the array)
4. Two Pointer (key is to understand pointer always start at begin and right, learn how to judge which pointer should move )
5. Stack (key is to understand how to using stack to find the most closest result)
6. Slide Window (key is to understand when to increase the window side and when to shrink the window size)
7. Binary Tree + BST Tree + Trie 
8. Graph (Key is to understand when to use Typological sort, and when to use Dijkstra's algorithm , SKIP UNION FIND  )
9. Greedy, Prefix Sum 


### Study Step
1. Casual read the https://neetcode.io/ question (skip dynamic program, bit wise type question) and skip hard question
2. Do all the easy first and the medium (Remember to watch the video)
3. Having a 20 weeks plan https://www.techinterviewhandbook.org/grind75?weeks=20 to follow the plan
4. Having a excel to record what question you have done, and things you learn, and note for the question, can you solve it or not, time spent on the question e.g(https://docs.google.com/spreadsheets/d/1YFpllJAj8OhfvxfQH9GLkkCJTdJNibhR6bV9sv0YhhU/edit#gid=0&fvid=1268885056)
5. For every weekend, revisit the question u dont understand or u cant solve
6. Try to find out a coding template for your self to solve different algorithm question
